package LabAssistant.ClientConnections;

import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.server.standard.ServerEndpointExporter;
import org.springframework.web.socket.server.support.DefaultHandshakeHandler;

/**
 * WebSocket configuration class
 * @author Andrew Phares
 *
 */
@ConditionalOnWebApplication
@Configuration
@EnableWebSocketMessageBroker
public class ClientConnectionsConf implements WebSocketMessageBrokerConfigurer {

	/**
	 * Create stomp endpoints
	 * @param registry Sets the endpoint
	 */
	@Override
	public void registerStompEndpoints(StompEndpointRegistry registry) {
	         
	registry
	  .addEndpoint("/greeting")
	  .setHandshakeHandler(new DefaultHandshakeHandler() {
	 
	      public boolean beforeHandshake(
	        ServerHttpRequest request, 
	        ServerHttpResponse response, 
	        WebSocketHandler wsHandler,
	        Map attributes) throws Exception {
	  
	            if (request instanceof ServletServerHttpRequest) {
	                ServletServerHttpRequest servletRequest
	                 = (ServletServerHttpRequest) request;
	                HttpSession session = servletRequest
	                  .getServletRequest().getSession();
	                attributes.put("sessionId", session.getId());
	            }
	                return true;
	        }}).withSockJS();
	    }

	/**
	 * Sets up message broadcasting for the websocket
	 * @param config the config registry
	 */
    @Override
    public void configureMessageBroker(MessageBrokerRegistry config) {
        config.enableSimpleBroker("/topic/","/queue/");
        config.setApplicationDestinationPrefixes("/app");
    }
    
    
    /**
     * Exports the WebSocket's endpoints
     * @return the EndPointExporter object
     */
    @Bean
    public ServerEndpointExporter serverEndpointExporter() {
    	return new ServerEndpointExporter();
    }

    /**
     * Implements custom configurator class
     * @return Config class for this websocket
     */
    @Bean
    public Config customConfigurator() {
    	return new Config();
    }
}