package LabAssistant;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;

/**
 * Assembles a professor as a Resource
 * @author Andrew Phares
 *
 */
@Component
class ProfResourceAssembler implements ResourceAssembler<Prof, Resource<Prof>> {

	/**
	 * Returns the Resource
	 * @param user The Professor entry to assemble
	 * @return assembled resource
	 */
	@Override
	public Resource<Prof> toResource(Prof user) {

		return new Resource<>(user,
			linkTo(methodOn(ProfTableController.class).one(user.getUsername())).withSelfRel(),
			linkTo(methodOn(ProfTableController.class).all()).withRel("Prof"));
	}
}