package LabAssistant;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
/**
 * The repository for the Student SQL table
 * @author Andrew Phares
 *
 */
@Repository
public interface StudentTableRepository extends JpaRepository<Student, String> {
	
	/**
	 * Saves the given Student entry to the database
	 * @param UserTable The new Student entry to load into the repository
	 */
	Student save (Student UserTable);
	
}