package LabAssistant;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
/**
 * The repository for the TA SQL table
 * @author Andrew Phares
 *
 */
@Repository
public interface TATableRepository extends JpaRepository<TA, String> {
	
	/**
	 * Saves the given TA entry to the database
	 * @param UserTable The new TA entry to load into the repository
	 */
	TA save (TA UserTable);
	
}