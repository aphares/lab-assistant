package LabAssistant;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * The repository for the TA SQL table
 * @author Andrew Phares
 *
 */
@Repository
public interface ProfTableRepository extends JpaRepository<Prof, String> {
	
	/**
	 * Saves the given Professor entry to the database
	 * @param UserTable The new Professor entry to load into the repository
	 */
	Prof save (Prof UserTable);
	
}