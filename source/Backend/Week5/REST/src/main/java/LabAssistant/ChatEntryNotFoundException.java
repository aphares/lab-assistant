package LabAssistant;

/**
 * Exception when this chat object isn't found on SQL database
 * @author Andrew Phares
 *
 */
class ChatEntryNotFoundException extends RuntimeException {

	/**
	 * Constructor called when chat entry not found
	 * @param id id of the chat entry queried
	 */
	ChatEntryNotFoundException(Long id) {
		super("Could not find ChatEntry " + id);
	}
}