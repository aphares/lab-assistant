package LabAssistant;

/**
 * Exception when this queue object isn't found on SQL database
 * @author Andrew Phares
 *
 */
public class QueueEntryNotFoundException extends RuntimeException {

	/**
	 * Constructor called when queue entry not found
	 * @param id username of the queue item queried
	 */
	public QueueEntryNotFoundException(String id) {
		super("Could not find QueueEntry " + id);
	}
}