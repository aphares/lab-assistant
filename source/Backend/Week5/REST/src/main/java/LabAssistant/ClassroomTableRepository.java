package LabAssistant;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
/**
 * The repository for the Classroom SQL table
 * @author Andrew Phares
 *
 */
@Repository
public interface ClassroomTableRepository extends JpaRepository<Classroom, String> {
	
	/**
	 * Saves the given Classroom entry to the database
	 * @param UserTable The new Classroom entry to load into the repository
	 */
	Classroom save (Classroom classtable);
	
}