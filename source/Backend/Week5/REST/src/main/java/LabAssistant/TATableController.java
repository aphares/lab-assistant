package LabAssistant;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.stream.Collectors;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.*;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

/**
 * Controller class that handles http requests POST, GET, PUT, and DEL for the TA Table
 * @author Andrew Phares
 *
 */
@RestController
class TATableController {
	@Autowired
	private final TATableRepository repository;
	@Autowired	
	private final UserTableRepository urepository;
	private final TAResourceAssembler assembler;
	private final UserResourceAssembler uassembler;

	/**
	 * Creates this Queue Controller
	 * @param repository the Queue Repository
	 * @param assembler the Queue Assembler
	 */
	TATableController(TATableRepository repository, TAResourceAssembler assembler, UserTableRepository urepository, UserResourceAssembler uassembler) {
		this.repository = repository;
		this.assembler = assembler;
		this.urepository = urepository;
		this.uassembler = uassembler;
	}

	// Aggregate root
	/**
	 * Gets all data in the database for TA
	 * @return all TA data in json format
	 */
	@GetMapping(path = "/tas", produces = MediaType.APPLICATION_JSON_VALUE)
	Resources<Resource<TA>> all() {
		
		List<Resource<TA>> users = repository.findAll().stream()
				.map(assembler::toResource)
				.collect(Collectors.toList());

			return new Resources<>(users,
				linkTo(methodOn(TATableController.class).all()).withSelfRel());
	}

	/**
	 * POST request - posts a new TA object to the database
	 * @param newUserEntry the TA object to add to the database
	 * @return the added TA data in json format
	 * @throws URISyntaxException error thrown when incorrect syntax is sent to server
	 */
	@PostMapping("/tas")
	ResponseEntity<?> newUserEntry(@RequestBody TA newUserEntry) throws URISyntaxException {
		Resource<TA> resource = assembler.toResource(repository.save(newUserEntry));
		setUserType(newUserEntry.getUsername(),false);
		return ResponseEntity
			.created(new URI(resource.getId().expand().getHref()))
			.body(resource);
	}

	// Single item
	/**
	 *  Returns the given TA data from the SQL database in json format
	 * @param id username of the TA entry
	 * @return the TA entry as a Resource
	 */
	@GetMapping(path = "/tas/{username}", produces = MediaType.APPLICATION_JSON_VALUE)
	Resource<TA> one(@PathVariable("username") String string) {

		TA users = repository.findById(string)
			.orElseThrow(() -> new TAEntryNotFoundException(string));

		return assembler.toResource(users);
	}

	/**
	 * PUT request: Updated an entry on the database with another TA entity
	 * @param newUserEntry The updated TA entry
	 * @param composite the id of this TA entry
	 * @return the updated TA
	 * @throws URISyntaxException error thrown when incorrect syntax is sent to server
	 */
	@PutMapping("/tas/{username}")
	ResponseEntity<?> replaceUserEntry(@RequestBody TA newUserEntry, @PathVariable("username") String id) throws URISyntaxException {

		TA updatedUser = repository.findById(id)
				.map(user -> {
					user.setUsername(newUserEntry.getUsername());
					return repository.save(user);
				})
				.orElseGet(() -> {
					newUserEntry.setUsername(id);
					return repository.save(newUserEntry);
				});

			Resource<TA> resource = assembler.toResource(updatedUser);
			setUserType(id,true);
			return ResponseEntity
				.created(new URI(resource.getId().expand().getHref()))
				.body(resource);
	}
	
	/**
	 * Deletes the specified entry from the TA table
	 * @param id entry to delete, with primary key "username"
	 * @return empty json
	 */
	@DeleteMapping("/tas/{username}")
	ResponseEntity<?> deleteUserEntry(@PathVariable("username") String id) {
		repository.deleteById(id);
		setUserType(id,true);
		return ResponseEntity.noContent().build();
	}
	
	/**
	 * Function to automatically set user type to 0 when deleting a TA's from the table, and to 2 when adding a TA's to the table.
	 * @id the TA's username
	 * @isDelete true if deleting from table, false if posting to table
	 */
	void setUserType(String id, boolean isDelete) {
		user_table newType = urepository.findById(id).get();
		if (isDelete) {
			newType.setType(0);
		}
		else {
			newType.setType(2);
		}
		
		user_table updatedUser = urepository.findById(id)
				.map(user -> {
					user.setUsername(newType.getUsername());
					return urepository.save(user);
				})
				.orElseGet(() -> {
					newType.setUsername(id);
					return urepository.save(newType);
				});
	}
}