package LabAssistant;

/**
 * Exception when this TA object isn't found on SQL database
 * @author Andrew Phares
 *
 */
class TAEntryNotFoundException extends RuntimeException {

	
	/**
	 * Constructor called when TA entry not found
	 * @param id username of the TA queried
	 */
	TAEntryNotFoundException(String id) {
		super("Could not find TA Entry " + id);
	}
}