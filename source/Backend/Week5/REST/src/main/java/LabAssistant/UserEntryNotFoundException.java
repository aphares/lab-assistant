package LabAssistant;

/**
 * Exception when this User object isn't found on SQL database
 * @author Andrew Phares
 *
 */
class UserEntryNotFoundException extends RuntimeException {

	/**
	 * Constructor called when entry not found
	 * @param username username of the user queried
	 */
	UserEntryNotFoundException(String username) {
		super("Could not find UserEntry " + username);
	}
}