package LabAssistant;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Throws a message when TA entry isn't found
 * @author Andrew Phares
 *
 */
@ControllerAdvice
class TAEntryNotFoundAdvice {

	/**
	 * Gets the error message
	 * @param ex Not found exception
	 * @return Exception message
	 */
	@ResponseBody
	@ExceptionHandler(TAEntryNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	String userNotFoundHandler(TAEntryNotFoundException ex) {
		return ex.getMessage();
	}
}