package LabAssistant;

/**
 * Exception when this professor object isn't found on SQL database
 * @author Andrew Phares
 *
 */
class ProfEntryNotFoundException extends RuntimeException {

	/**
	 * Constructor called when Professor entry not found
	 * @param id username of the professor queried
	 */
	ProfEntryNotFoundException(String id) {
		super("Could not find professor Entry " + id);
	}
}