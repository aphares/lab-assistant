package LabAssistant;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.*;

/**
 * SQL Table - Professor Table
 * Stores the data for Professor users of Lab Assistant
 * @author Andrew Phares
 *
 */

@Data
@Entity
@Table(name = "professor")
@XmlRootElement(name = "ProfTable")
@XmlAccessorType(XmlAccessType.FIELD)
public class Prof {

	/**
	 * Primary key & username for a Professor entry
	 */
	@Id @XmlElement(name = "username")
	private String username;
	
	/**
	 * joins this Student object with a user object joined on column username
	 */
	 @OneToOne()
	 @JoinColumn(name="username")
	 private user_table user;

	 /**
	 * Default Constructor
	 */
	public Prof(){
	}

	/**
	 * Constructor for this Student
	 * @param username this Professor's username
	 */
	public Prof(String username) {
			this.username = username;
		}
	}
