package LabAssistant;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;

/**
 * Assembles a user as a Resource
 * @author Andrew Phares
 *
 */
@Component
class UserResourceAssembler implements ResourceAssembler<user_table, Resource<user_table>> {

	/**
	 * Returns the Resource
	 * @param user The User entry to assemble
	 * @return assembled resource
	 */
	@Override
	public Resource<user_table> toResource(user_table user) {

		return new Resource<>(user,
			linkTo(methodOn(UserTableController.class).one(user.getUsername())).withSelfRel(),
			linkTo(methodOn(UserTableController.class).all()).withRel("users"));
	}
}