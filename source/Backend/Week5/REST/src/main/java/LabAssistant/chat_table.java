package LabAssistant;

import lombok.Data;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.*;

/**
 * SQL Table - Chat Table
 * Stores the data for the chat feature of Lab Assistant
 * @author Andrew Phares
 *
 */

@Data
@Entity
@Table(name = "ChatTable")
@XmlRootElement(name = "chatData")
@XmlAccessorType (XmlAccessType.FIELD)
public class chat_table {

	/**
	 * Primary key for a chat entry
	 */
	private @Id @GeneratedValue(strategy = GenerationType.IDENTITY) Long id;
	
	/**
	 * The netid of the user who posted this chat entry
	 */
	@XmlElement(name = "netid")
	private String netid;
	
	/**
	 * Message of this chat entry
	 */
	@XmlElement(name = "message")
	private String message;

	/**
	 * Time this chat entry was posted
	 */
	@XmlElement(name = "timestamp")
	private String timestamp;
	
	/**
	 * Default Constructor
	 */	
	public chat_table(){
		setTime();
	}
	
	/**
	 * Constructor for a chat entry
	 * @param username the user who posts the chat message
	 * @param chattext the message of the chat entry
	 */
	public chat_table(String username, String chattext) {
		setTime();
		this.netid = username;
		this.message = chattext;
	}
	
	/**
	 * Sets the timestamp based on when the server receives a new chat entry
	 */
	public void setTime() {
	    DateFormat df = new SimpleDateFormat("HH:mm");
	    Date dateobj = new Date();
	    timestamp = df.format(dateobj);
	    if (Integer.valueOf(timestamp.substring(0,2)) > 12) {
	    	timestamp = String.valueOf(Integer.valueOf(timestamp.substring(0,2)) - 12) + timestamp.substring(2) + " PM";
	    }
	    else if (Integer.valueOf(timestamp.substring(0,2)) == 12) {
	    	timestamp+= " PM";
	    }
	    else if (Integer.valueOf(timestamp.substring(0,2)) == 0) {
	    	timestamp = "12" + timestamp.substring(2) + " AM";
	    }
	    else {
	    	timestamp+= " AM";
	    }
	}
}