package LabAssistant;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.stream.Collectors;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.*;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

/**
 * Controller class that handles http requests POST, GET, PUT, and DEL for the Professors Table
 * @author Andrew Phares
 *
 */
@RestController
class ProfTableController {
	@Autowired	
	private final ProfTableRepository repository;
	
	@Autowired	
	private final UserTableRepository urepository;
	
	private final ProfResourceAssembler assembler;
	private final UserResourceAssembler uassembler;

	/**
	 * Creates this Queue Controller
	 * @param repository the Queue Repository
	 * @param assembler the Queue Assembler
	 */
	ProfTableController(ProfTableRepository repository, ProfResourceAssembler assembler, UserTableRepository urepository, UserResourceAssembler uassembler) {
		this.repository = repository;
		this.assembler = assembler;
		this.urepository = urepository;
		this.uassembler = uassembler;
	}

	// Aggregate root
	/**
	 * Gets all data in the database for Professor
	 * @return all Professor data in json format
	 */
	@GetMapping(path = "/professors", produces = MediaType.APPLICATION_JSON_VALUE)
	Resources<Resource<Prof>> all() {
		
		List<Resource<Prof>> users = repository.findAll().stream()
				.map(assembler::toResource)
				.collect(Collectors.toList());

			return new Resources<>(users,
				linkTo(methodOn(ProfTableController.class).all()).withSelfRel());
	}

	/**
	 * POST request - posts a new Professor object to the database
	 * @param newUserEntry the Professor object to add to the database
	 * @return the added Professor data in json format
	 * @throws URISyntaxException error thrown when incorrect syntax is sent to server
	 */
	@PostMapping("/professors")
	ResponseEntity<?> newUserEntry(@RequestBody Prof newUserEntry) throws URISyntaxException {
		Resource<Prof> resource = assembler.toResource(repository.save(newUserEntry));
		setUserType(newUserEntry.getUsername(),false);
		return ResponseEntity
			.created(new URI(resource.getId().expand().getHref()))
			.body(resource);
	}

	// Single item
	/**
	 *  Returns the given professor data from the SQL database in json format
	 * @param id username of the professor entry
	 * @return the professor entry as a Resource
	 */
	@GetMapping(path = "/professors/{username}", produces = MediaType.APPLICATION_JSON_VALUE)
	Resource<Prof> one(@PathVariable("username") String id) {

		Prof users = repository.findById(id)
			.orElseThrow(() -> new ProfEntryNotFoundException(id));

		return assembler.toResource(users);
	}

	/**
	 * PUT request: Updated an entry on the database with another classroom entity
	 * @param newUserEntry The updated classroom entry
	 * @param composite the id of this classroom entry
	 * @return the updated classroom item
	 * @throws URISyntaxException error thrown when incorrect syntax is sent to server
	 */
	@PutMapping("/professors/{username}")
	ResponseEntity<?> replaceUserEntry(@RequestBody Prof newUserEntry, @PathVariable("username") String id) throws URISyntaxException {

		Prof updatedUser = repository.findById(id)
				.map(user -> {
					user.setUsername(newUserEntry.getUsername());
					return repository.save(user);
				})
				.orElseGet(() -> {
					newUserEntry.setUsername(id);
					return repository.save(newUserEntry);
				});

			Resource<Prof> resource = assembler.toResource(updatedUser);

			return ResponseEntity
				.created(new URI(resource.getId().expand().getHref()))
				.body(resource);
	}

	/**
	 * Deletes the specified entry from the Professor table
	 * @param id entry to delete, with primary key "username"
	 * @return empty json
	 */
	@DeleteMapping("/professors/{username}")
	ResponseEntity<?> deleteUserEntry(@PathVariable("username") String id) {
		repository.deleteById(id);
		setUserType(id,true);
		return ResponseEntity.noContent().build();
	}
	
	/**
	 * Function to automatically set user type to 0 when deleting a professor from the table, and to 1 when adding a professor to the table.
	 * @id the professor's username
	 * @isDelete true if deleting from table, false if posting to table
	 */
	void setUserType(String id, boolean isDelete) {
		user_table newType = urepository.findById(id).get();
		if (isDelete) {
			newType.setType(0);
		}
		else {
			newType.setType(1);
		}
		
		user_table updatedUser = urepository.findById(id)
				.map(user -> {
					user.setUsername(newType.getUsername());
					return urepository.save(user);
				})
				.orElseGet(() -> {
					newType.setUsername(id);
					return urepository.save(newType);
				});
	}
}