package LabAssistant;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.stream.Collectors;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.*;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

/**
 * Controller class that handles http requests POST, GET, PUT, and DEL for the Classroom Table
 * @author Andrew Phares
 *
 */
@RestController
public
class ClassroomTableController {
	@Autowired
	
	private final ClassroomTableRepository repository;
	
	private final ClassroomResourceAssembler assembler;

	/**
	 * Creates this Queue Controller
	 * @param repository the Queue Repository
	 * @param assembler the Queue Assembler
	 */
	ClassroomTableController(ClassroomTableRepository repository, ClassroomResourceAssembler assembler) {
		this.repository = repository;
		this.assembler = assembler;
	}

	// Aggregate root
	/**
	 * Gets all data in the database for Classroom
	 * @return all Classroom data in json format
	 */
	@GetMapping(path = "/classes", produces = MediaType.APPLICATION_JSON_VALUE)
	Resources<Resource<Classroom>> all() {
		
		List<Resource<Classroom>> users = repository.findAll().stream()
				.map(assembler::toResource)
				.collect(Collectors.toList());

			return new Resources<>(users,
				linkTo(methodOn(ClassroomTableController.class).all()).withSelfRel());
	}

	/**
	 * POST request - posts a new Classroom object to the database
	 * @param newClassroomEntry the Classroom object to add to the database
	 * @return the added Classroom data in json format
	 * @throws URISyntaxException error thrown when incorrect syntax is sent to server
	 */
	@PostMapping("/classes")
	ResponseEntity<?> newClassroomEntry(@RequestBody Classroom newClassroomEntry) throws URISyntaxException {
		Resource<Classroom> resource = assembler.toResource(repository.save(newClassroomEntry));

		return ResponseEntity
			.created(new URI(resource.getId().expand().getHref()))
			.body(resource);
	}

	// Single item
	/**
	 *  Returns the given classroom data from the SQL database in json format
	 * @param id name of the classroom entry
	 * @return the classroom entry as a Resource
	 */
	@GetMapping(path = "/classes/{name}", produces = MediaType.APPLICATION_JSON_VALUE)
	public
	Resource<Classroom> one(@PathVariable("name") String id) {

		Classroom users = repository.findById(id)
			.orElseThrow(() -> new ClassroomEntryNotFoundException(id));

		return assembler.toResource(users);
	}

	/**
	 * PUT request: Updated an entry on the database with another classroom entity
	 * @param newClassroomEntry The updated classroom entry
	 * @param composite the id of this classroom entry
	 * @return the updated classroom item
	 * @throws URISyntaxException error thrown when incorrect syntax is sent to server
	 */
	@PutMapping("/classes/{name}")
	ResponseEntity<?> replaceClassroomEntry(@RequestBody Classroom newClassroomEntry, @PathVariable("username") String id) throws URISyntaxException {

		Classroom updatedUser = repository.findById(id)
				.map(user -> {
					user.setName(newClassroomEntry.getName());
					return repository.save(user);
				})
				.orElseGet(() -> {
					newClassroomEntry.setName(id);
					return repository.save(newClassroomEntry);
				});

			Resource<Classroom> resource = assembler.toResource(updatedUser);

			return ResponseEntity
				.created(new URI(resource.getId().expand().getHref()))
				.body(resource);
	}
	
	/**
	 * Deletes the specified entry from the Classroom table
	 * @param id entry to delete, with primary key "username"
	 * @return empty json
	 */
	@DeleteMapping("/classes/{name}")
	ResponseEntity<?> deleteClassroomEntry(@PathVariable("name") String id) {
		repository.deleteById(id);

		return ResponseEntity.noContent().build();
	}
}