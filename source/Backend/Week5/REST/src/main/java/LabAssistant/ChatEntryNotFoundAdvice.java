package LabAssistant;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Throws a message when chat entry isn't found
 * @author Andrew Phares
 *
 */
@ControllerAdvice
class ChatEntryNotFoundAdvice {

	/**
	 * Gets the error message
	 * @param ex Not found exception
	 * @return Exception message
	 */
	@ResponseBody
	@ExceptionHandler(ChatEntryNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	String chatNotFoundHandler(ChatEntryNotFoundException ex) {
		return ex.getMessage();
	}
}