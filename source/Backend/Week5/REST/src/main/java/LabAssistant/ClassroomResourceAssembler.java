package LabAssistant;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;

/**
 * Assembles a classroom entry as a Resource
 * @author Andrew Phares
 *
 */
@Component
public
class ClassroomResourceAssembler implements ResourceAssembler<Classroom, Resource<Classroom>> {

	/**
	 * Returns the Resource
	 * @param theclass The classroom entry to assemble
	 * @return assembled resource
	 */
	@Override
	public Resource<Classroom> toResource(Classroom theclass) {

		return new Resource<>(theclass,
			linkTo(methodOn(ClassroomTableController.class).one(theclass.getName())).withSelfRel(),
			linkTo(methodOn(ClassroomTableController.class).all()).withRel("Classroom"));
	}
}