package LabAssistant;

import java.util.ArrayList;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
/**
 * Main Class that launches the springboot application
 * @author Andrew Phares
 *
 */
@SpringBootApplication
public class LabAssistantApplication {

	/**
	 * Main method - launches the Springboot application
	 * @param args no flags configured, all args ignored
	 */
	public static void main(String... args) {
		SpringApplication.run(LabAssistantApplication.class, args);
	}
}