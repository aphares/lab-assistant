package LabAssistant;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;

/**
 * Assembles a queue entry as a Resource
 * @author Andrew Phares
 *
 */
@Component
public
class QueueResourceAssembler implements ResourceAssembler<Queue, Resource<Queue>> {

	
	/**
	 * Returns the Resource
	 * @param user The Queue entry to assemble
	 * @return assembled resource
	 */
	@Override
	public Resource<Queue> toResource(Queue user) {

		return new Resource<>(user,
			linkTo(methodOn(QueueTableController.class).one(user.getComposite())).withSelfRel(),
			linkTo(methodOn(QueueTableController.class).all()).withRel("queue"));
	}
}