package LabAssistant;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.*;
/**
 * SQL Table - Users Table
 * Stores the data for base users of Lab Assistant
 * @author Andrew Phares
 *
 */
@Data
@Entity
@Table(name = "user_table")
@XmlRootElement(name = "UserTable")
@XmlAccessorType(XmlAccessType.FIELD)
public class user_table {
	
	/**
	 * Primary key username for a user entry
	 */
	@Id @XmlElement(name = "username")
	private String username;

	/**
	 * Password for this user
	 */
	@XmlElement(name = "password")
	private String password;

	/**
	 * This user's first name
	 */
	@XmlElement(name = "firstname")
	private String firstname;
	
	/**
	 * This user's last name
	 */
	@XmlElement(name = "lastname")
	private String lastname;
	
	/**
	 * This user's email
	 */
	@XmlElement(name = "email")
	private String email;
	
	/**
	 * Student's 9-digit id
	 */
	@XmlElement(name = "id")
	private int id;
	
	/**
	 * Integer from 0-3 to set this user's type (Professor, Student, TA)
	 */
	@Column(name = "type")
	private int type;
	
	/**
	 * Default Constructor
	 */
	public user_table(){
	}
	
	/**
	 * Constructor for user table
	 * @param username this user's username
	 * @param password the password
	 * @param firstname this user's first name
	 * @param lastname this user's last name
	 * @param Email this user's email
	 * @param id this user's 9-digit id
	 */
	public user_table(String username, String password, String firstname, String lastname, String Email, int id) {

		this.username = username;
		this.password = password;
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = Email;
		this.id = id;
	}
	
	/**
	 * Constructor for user table
	 * @param username this user's username
	 * @param password the password
	 * @param firstname this user's first name
	 * @param lastname this user's last name
	 * @param Email this user's email
	 */
	public user_table(String username, String password, String firstname, String lastname, String Email) {

		this.username = username;
		this.password = password;
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = Email;
	}
	}
