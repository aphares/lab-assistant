package LabAssistant;

import lombok.Data;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.*;
/**
 * SQL Table - Classroom Table
 * Stores the data for the classroom table of Lab Assistant
 * @author Andrew Phares
 *
 */
@Data
@Entity
@Table(name = "classroom")
@XmlRootElement(name = "ClassroomTable")
@XmlAccessorType(XmlAccessType.FIELD)
public class Classroom {

	/**
	 * Name of the classroom – primary key for a classroom entry
	 */
	@Id @XmlElement(name = "name")
	private String name;
	
	/**
	 * The first checkpoint
	 */
	@XmlElement(name = "checkpoint1")
	private String checkpoint1;
	
	/**
	 * The second checkpoint
	 */
	@XmlElement(name = "checkpoint2")
	private String checkpoint2;
	
	/**
	 * MM-DD-YYYY date
	 */
	@XmlElement(name = "date")
	private String date;

	/**
	 * Default Constructor
	 */
	public Classroom(){
	}
/**
 * 
 * @param name The name of this classroom
 * @param checkpoint1 The first checkpoint
 * @param checkpoint2 The second checkpoint
 * @param date MM-DD-YYYY date
 */
	public Classroom(String name, String checkpoint1, String checkpoint2, String date) {
			this.name = name;
			this.checkpoint1 = checkpoint1;
			this.checkpoint2 = checkpoint2;
			this.date = date;
		}
	}
