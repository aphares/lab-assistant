package LabAssistant.ClientConnections;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.hateoas.Resource;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;

import LabAssistant.Classroom;
import LabAssistant.ClassroomResourceAssembler;
import LabAssistant.ClassroomTableRepository;
import LabAssistant.Queue;
import LabAssistant.QueueEntryNotFoundException;
import LabAssistant.QueueResourceAssembler;
import LabAssistant.QueueTableRepository;
import LabAssistant.StudentTableRepository;
import LabAssistant.UserTableRepository;

/**
 * Handles client connections and server behavior using websockets.
 * @author Andrew Phares
 *
 */

@ServerEndpoint(value = "/LAWebsockets/{userid}", configurator = Config.class)
@Component
public class ClientConnectionsServer {
	
	/**
	 * accesses/modifies queue table resources.
	 */
	@Autowired
	private final QueueTableRepository QueueRepository;
	
	/**
	 * accesses/modifies classroom table resources.
	 */
	@Autowired
	private final ClassroomTableRepository ClassroomRepository;

	/**
	 * accesses/modifies student table resources.
	 */
	@Autowired
	private final UserTableRepository StudentRepository;
	
	/**
	 * assembles queue table resources.
	 */
	private final QueueResourceAssembler QueueAssembler;
	
	/**
	 * assembles classroom table resources.
	 */
	private final ClassroomResourceAssembler ClassroomAssembler;
	
	/**
	 * Constructor for the websocket server and behavior.
	 * @param repository accesses/modifies classroom table resources.
	 * @param QueueAssembler assembles queue table resources.
	 * @param ClassroomRepository accesses/modifies classroom table resources.
	 * @param ClassroomAssembler assembles classroom table resources.
	 * @param student accesses/modifies student table resources.
	 */
	public ClientConnectionsServer(QueueTableRepository repository, QueueResourceAssembler QueueAssembler, 
			ClassroomTableRepository ClassroomRepository, ClassroomResourceAssembler ClassroomAssembler, UserTableRepository student) {
		this.QueueRepository = repository;
		this.QueueAssembler = QueueAssembler;
		this.ClassroomAssembler = ClassroomAssembler;
		this.ClassroomRepository = ClassroomRepository;
		this.StudentRepository = student;
	}
	
	
	// Store all socket session and their corresponding username.
	/**
	 * Session hashmap
	 */
    private static Map<Session, String> sessionUsernameMap = new HashMap<>();
    /**
     * Username hashmap
     */
    private static Map<String, Session> usernameSessionMap = new HashMap<>();
    
    /**
     * Logs network info to server console
     */
    private final Logger logger = LoggerFactory.getLogger(ClientConnectionsServer.class);
    
    /**
     * 
     * @param session the session of the connecting user.
     * @param username the username of the connecting user.
     * @throws IOException if session is invalid.
     */
    @OnOpen
    public void onOpen(Session session, @PathParam("userid") String username) throws IOException 
    {
        logger.info("Launching onOpen");
        sessionUsernameMap.put(session, username);
        usernameSessionMap.put(username, session);
        String message="User:" + username + " has connected";
        broadcast(message);
    }
 
    /**
     * Receives a message as string from clients. 
     * @param session the session that sent the message.
     * @param message message received.
     * @throws IOException if session is invalid.
     */
    @OnMessage
    public void onMessage(Session session, String message) throws IOException 
    {
        // Handle new messages1
    	if (message.compareTo("queue") == 0) {
    		String reply = "";
    		List<Resource<Queue>> users = QueueRepository.findAll().stream()
    				.map(QueueAssembler::toResource)
    				.collect(Collectors.toList());
    		
    		Gson rep = new Gson();  		
    		session.getBasicRemote().sendText(rep.toJson(users));
    		
    		
    	}
        // Handle new messages1
    	if (message.compareTo("classroom") == 0) {
    		String reply = "";
    		List<Resource<Classroom>> users = ClassroomRepository.findAll().stream()
    				.map(ClassroomAssembler::toResource)
    				.collect(Collectors.toList());
    		
    		Gson rep = new Gson();  		
    		session.getBasicRemote().sendText(rep.toJson(users));
    		
    		
    	}
		
    	if (message.length() > 9 && message.substring(0,9).compareTo("AddQueue:") == 0) {


    		
    		String username = message.substring(9,message.indexOf(','));

    		Queue newItem = new Queue(username, message.substring(username.length()+10), username + message.substring(username.length()+10));
    		
    		List<Resource<Queue>> users = QueueRepository.findAll().stream()
    				.map(QueueAssembler::toResource)
    				.collect(Collectors.toList());
    		int i = users.size();
    		newItem.setPriority(i+1);
    		QueueRepository.save(newItem);
    		
    		users = QueueRepository.findAll().stream()
    				.map(QueueAssembler::toResource)
    				.collect(Collectors.toList());
    	
    		Gson rep = new Gson();  		
    		broadcast(rep.toJson(users));
    	}
    	
    	if (message.length() > 9 && message.substring(0,9).compareTo("DelQueue:") == 0) {
    		
    		String username = message.substring(9,message.indexOf(','));
    		 
    		QueueRepository.deleteById(username + message.substring(username.length()+10));  				
    		List<Queue> users = QueueRepository.findAll();  
    		Queue tmp;
    		
    		for (int i = 0; i < users.size(); i++) {
    	    	tmp = users.get(i);
    	    	tmp.setPriority(i+1);
    	    	QueueRepository.deleteById(users.get(i).getComposite());
    	    	QueueRepository.save(tmp);
    		}
    		
    		List<Resource<Queue>> newusers = QueueRepository.findAll().stream()
    				.map(QueueAssembler::toResource)
    				.collect(Collectors.toList());
    		
    		Gson rep = new Gson();  		
    		broadcast(rep.toJson(newusers));

    	}
    	
    	if (message.compareTo("close:") == 0) {
    			onClose(session);
    	}

    }

    /**
     * Called when server closes the connection with this session.
     * @param session session to close.
     * @throws IOException if session is invalid.
     */
    @OnClose
    public void onClose(Session session) throws IOException
    {
    	logger.info("Entered into Close");
    	String username = sessionUsernameMap.get(session);
    	sessionUsernameMap.remove(session);
    	usernameSessionMap.remove(username);
        
        broadcast(username + " disconnected");
    }
 
    /**
     * Sends error messsage to server when an issue with clients closes connections.
     * @param session session where error occurred.
     * @param throwable type of error to log to server.
     */
    @OnError
    public void onError(Session session, Throwable throwable) 
    {
    	logger.info("Error, connection closed.");
    }
    
    /**
     * Sends message to specified user.
     * @param username user to send the message to.
     * @param message message to send
     */
	private void sendMessageToPArticularUser(String username, String message) 
    {	
    	try {
    		usernameSessionMap.get(username).getBasicRemote().sendText(message);
        } catch (IOException e) {
        	logger.info("Exception: " + e.getMessage().toString());
            e.printStackTrace();
        }
    }
    
	/**
	 * Broadcasts message to all connected users.
	 * @param message message to broadcast
	 * @throws IOException if session is invalid.
	 */
    private static void broadcast(String message) 
    	      throws IOException 
    {	  
    	sessionUsernameMap.forEach((session, username) -> {
    		synchronized (session) {
	            try {
	                session.getBasicRemote().sendText(message);
	            } catch (IOException e) {
	                e.printStackTrace();
	            }
	        }
	    });
	}
}

