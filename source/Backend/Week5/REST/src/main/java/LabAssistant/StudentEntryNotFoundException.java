package LabAssistant;

/**
 * Exception when this student object isn't found on SQL database
 * @author Andrew Phares
 *
 */
class StudentEntryNotFoundException extends RuntimeException {

	/**
	 * Constructor called when Student entry not found
	 * @param id username of the Student queried
	 */
	StudentEntryNotFoundException(String id) {
		super("Could not find StudentEntry " + id);
	}
}