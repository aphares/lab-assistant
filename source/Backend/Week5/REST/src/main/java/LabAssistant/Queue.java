package LabAssistant;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.*;

import org.hibernate.SessionFactory;
import org.hibernate.annotations.ManyToAny;
import org.hibernate.annotations.NamedQuery;

/**
 * SQL Table - Queue Table
 * Stores the data for the Queue feature of Lab Assistant
 * @author Andrew Phares
 *
 */

@Data
@Entity
@Table(name = "queue_table")
@NamedQuery(name = "Queue.findAll", query="select q from Queue q order by q.priority ASC")
@XmlRootElement(name = "QueueTable")
@XmlAccessorType(XmlAccessType.FIELD)
public class Queue {

	/**
	 * Composite id: composed of username+reason
	 */
	@Id
	@XmlElement(name = "composite")
	@Column(name = "composite")
	private String composite;

	/**
	 * username for a queue entry
	 */
	@XmlElement(name = "username")
	@Column(name = "username")
	private String username;
	
	/**
	 * reason - can be "checkoff" or "question"
	 */
	@XmlElement(name = "reason")
	@Column(name = "reason")
	private String reason;
	
	/**
	 * priority - set based on where Queue is in list
	 */
	@XmlElement(name = "priority")
	@Column(name = "priority")
	private int priority;
	
	/**
	 * Queue is joined ManyToOne to the student table
	 */
	 @ManyToOne()
	 @JoinColumn(name="username", insertable = false, updatable = false)
	 private user_table student;
	 
	 /**
	  * This object's SessionFactory
	  */
	private static SessionFactory factory;

	/**
	 * Default constructor that sets the default value for priority, 1
	 */
	public Queue() {
		priority = 1;
	}

	/**
	 * Constructs the queue object - sets the default value for priority, 1
	 * @param user the username of the user posting the entry
	 * @param reason the reason for the user posting the entry
	 * @param composite the compositie id
	 */
	public Queue(String user, String reason, String composite) {
		this.username = user;
		this.reason = reason;
		this.composite = composite;
		priority = 1;
	}
/**
	public void incrementPriority() {

		Session session = factory.openSession();
		Transaction tx = null;

		try {
			// Define a list of students and tas
			tx = session.beginTransaction();
			List<TA> taList = (List<TA>) session.createQuery("FROM TA").list();
			List<Queue> studentList = (List<Queue>) session.createQuery("FROM Student").list();
			Iterator iterator1 = taList.iterator();
	
			int count = Integer.MAX_VALUE;
			int temp;
			int section = 0;
			//find the lowest number of students in a section
			while (iterator1.hasNext()) {
					temp = 0;
					TA ta = (TA) iterator1.next();				
					Iterator iterator2 = studentList.iterator();
					
					while (iterator2.hasNext()) {
						Queue student = (Queue) iterator2.next();
						if (student.getSection() == ta.getSection())
							temp++;
					}
					if (temp < count) {
						count = temp;
						section = ta.getSection();
					}
			}
			
			this.setSection(section);
			tx.commit();
			
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}

	}
**/
}
