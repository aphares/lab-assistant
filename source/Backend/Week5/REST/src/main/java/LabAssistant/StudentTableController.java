package LabAssistant;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.stream.Collectors;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.*;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

/**
 * Controller class that handles http requests POST, GET, PUT, and DEL for the Student Table
 * @author Andrew Phares
 *
 */
@RestController
class StudentTableController {
	@Autowired
	private final StudentTableRepository repository;
	@Autowired	
	private final UserTableRepository urepository;
	private final StudentResourceAssembler assembler;
	private final UserResourceAssembler uassembler;

	/**
	 * Creates this Queue Controller
	 * @param repository the Queue Repository
	 * @param assembler the Queue Assembler
	 */
	StudentTableController(StudentTableRepository repository, StudentResourceAssembler assembler, UserTableRepository urepository, UserResourceAssembler uassembler) {
		this.repository = repository;
		this.assembler = assembler;
		this.urepository = urepository;
		this.uassembler = uassembler;
	}

	// Aggregate root
	/**
	 * Gets all data in the database for Student
	 * @return all Student data in json format
	 */
	@GetMapping(path = "/students", produces = MediaType.APPLICATION_JSON_VALUE)
	Resources<Resource<Student>> all() {
		
		List<Resource<Student>> users = repository.findAll().stream()
				.map(assembler::toResource)
				.collect(Collectors.toList());

			return new Resources<>(users,
				linkTo(methodOn(StudentTableController.class).all()).withSelfRel());
	}

	/**
	 * POST request - posts a new Student object to the database
	 * @param newUserEntry the Student object to add to the database
	 * @return the added Student data in json format
	 * @throws URISyntaxException error thrown when incorrect syntax is sent to server
	 */
	@PostMapping("/students")
	ResponseEntity<?> newUserEntry(@RequestBody Student newUserEntry) throws URISyntaxException {
		Resource<Student> resource = assembler.toResource(repository.save(newUserEntry));
		setUserType(newUserEntry.getUsername(),false);
		return ResponseEntity
			.created(new URI(resource.getId().expand().getHref()))
			.body(resource);
	}

	// Single item
	/**
	 *  Returns the given student data from the SQL database in json format
	 * @param username username of the student entry
	 * @return the student entry as a Resource
	 */
	@GetMapping(path = "/students/{username}", produces = MediaType.APPLICATION_JSON_VALUE)
	Resource<Student> one(@PathVariable("username") String username) {

		Student users = repository.findById(username)
			.orElseThrow(() -> new StudentEntryNotFoundException(username));

		return assembler.toResource(users);
	}
	
	/**
	 * PUT request: Updated an entry on the database with another student entity
	 * @param newUserEntry The updated student entry
	 * @param composite the id of this student entry
	 * @return the updated student
	 * @throws URISyntaxException error thrown when incorrect syntax is sent to server
	 */
	@PutMapping("/students/{username}")
	ResponseEntity<?> replaceUserEntry(@RequestBody Student newUserEntry, @PathVariable("username") String username) throws URISyntaxException {

		Student updatedUser = repository.findById(username)
				.map(user -> {
					user.setUsername(newUserEntry.getUsername());
					return repository.save(user);
				})
				.orElseGet(() -> {
					newUserEntry.setUsername(username);
					return repository.save(newUserEntry);
				});

			Resource<Student> resource = assembler.toResource(updatedUser);

			return ResponseEntity
				.created(new URI(resource.getId().expand().getHref()))
				.body(resource);
	}

	/**
	 * Deletes the specified entry from the Student table
	 * @param username entry to delete, with primary key "username"
	 * @return empty json
	 */
	@DeleteMapping("/students/{username}")
	ResponseEntity<?> deleteUserEntry(@PathVariable("username") String username) {
		repository.deleteById(username);
		setUserType(username,true);
		return ResponseEntity.noContent().build();
	}
	
	/**
	 * Function to automatically set user type to 0 when deleting a student from the table, and to 3 when adding a student to the table.
	 * @id the student's username
	 * @isDelete true if deleting from table, false if posting to table
	 */
	void setUserType(String id, boolean isDelete) {
		user_table newType = urepository.findById(id).get();
		if (isDelete) {
			newType.setType(0);
		}
		else {
			newType.setType(3);
		}
		
		user_table updatedUser = urepository.findById(id)
				.map(user -> {
					user.setUsername(newType.getUsername());
					return urepository.save(user);
				})
				.orElseGet(() -> {
					newType.setUsername(id);
					return urepository.save(newType);
				});
	}
}