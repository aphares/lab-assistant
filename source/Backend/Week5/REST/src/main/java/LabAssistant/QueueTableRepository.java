package LabAssistant;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
/**
 * The repository for the Queue SQL table
 * @author Andrew Phares
 *
 */
@Repository
public interface QueueTableRepository extends JpaRepository<Queue, String> {
	
	/**
	 * Saves the given Queue entry to the database
	 * @param QueueTable The new Queue entry to load into the repository
	 */
	Queue save (Queue QueueTable);
	
	/**
	 * Returns all queue objects in database as a List of Resources
	 */
	@Override
	@Query
	public List<Queue> findAll();
}