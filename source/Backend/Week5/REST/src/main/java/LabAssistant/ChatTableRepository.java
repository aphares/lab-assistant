package LabAssistant;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
/**
 * The repository for the Chat SQL table
 * @author Andrew Phares
 *
 */
@Repository
public interface ChatTableRepository extends JpaRepository<chat_table, Long> {
	
	/**
	 * Saves the given Chat entry to the database
	 * @param UserTable The new Chat entry to load into the repository
	 */
	chat_table save (chat_table ChatTable);
	
}