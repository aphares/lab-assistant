package LabAssistant;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
/**
 * The repository for the User SQL table
 * @author Andrew Phares
 *
 */
@Repository
public interface UserTableRepository extends JpaRepository<user_table, String> {
	
	/**
	 * Saves the given User entry to the database
	 * @param UserTable The new User entry to load into the repository
	 */
	user_table save (user_table UserTable);
	
}