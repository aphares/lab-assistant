package LabAssistant;

/**
 * Exception when this classroom object isn't found on SQL database
 * @author Andrew Phares
 *
 */
class ClassroomEntryNotFoundException extends RuntimeException {

	/**
	 * Constructor called when classroom entry not found
	 * @param id name of the classroom queried
	 */
	ClassroomEntryNotFoundException(String id) {
		super("Could not find classroom Entry " + id);
	}
}