package LabAssistant;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.stream.Collectors;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.hateoas.*;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.handler.annotation.SendTo;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

/**
 * Controller class that handles http requests POST, GET, PUT, and DEL for the Queue Table
 * A Queue entry's primary key is a composite - composite = username + reason
 * @author Andrew Phares
 *
 */
@RestController
public class QueueTableController {
	
	@Autowired
	private final QueueTableRepository repository;
	
	private final QueueResourceAssembler assembler;

	/**
	 * Creates this Queue Controller
	 * @param repository the Queue Repository
	 * @param assembler the Queue Assembler
	 */
	QueueTableController(QueueTableRepository repository, QueueResourceAssembler assembler) {
		this.repository = repository;
		this.assembler = assembler;
	}

	// Aggregate root
	/**
	 * Gets all data in the database for Queue
	 * @return all Queue data in json format
	 */
	@GetMapping(path = "/queue", produces = MediaType.APPLICATION_JSON_VALUE)
	public
	Resources<Resource<Queue>> all() {
		
		List<Resource<Queue>> users = repository.findAll().stream()
				.map(assembler::toResource)
				.collect(Collectors.toList());

			return new Resources<>(users,
				linkTo(methodOn(QueueTableController.class).all()).withSelfRel());
	}
	/**
	 * POST request - posts a new Queue object to the database
	 * @param newQueueEntry the Queue object to add to the database
	 * @return the added Queue data in json format
	 * @throws URISyntaxException error thrown when incorrect syntax is sent to server
	 */
	@PostMapping("/queue")
	ResponseEntity<?> newQueueEntry(@RequestBody Queue newQueueEntry) throws URISyntaxException {
		Resource<Queue> resource = assembler.toResource(repository.save(newQueueEntry));

		return ResponseEntity
			.created(new URI(resource.getId().expand().getHref()))
			.body(resource);
	}

	// Single item
	/**
	 * Returns the given queue data from the SQL database in json format
	 * @param composite primary key of the queue entry (username + reason)
	 * @return the queue entry as a Resource
	 */
	@GetMapping(path = "/queue/{composite}", produces = MediaType.APPLICATION_JSON_VALUE)
	public
	Resource<Queue> one(@PathVariable("composite") String composite) {

		Queue users = repository.findById(composite)
			.orElseThrow(() -> new QueueEntryNotFoundException(composite));

		return assembler.toResource(users);
	}
	
	/**
	 * PUT request: Updated an entry on the database with another Queue entity
	 * @param newQueueEntry The updated Queue entry
	 * @param composite the id of this Queue entry (username + reason)
	 * @return the updated Queue entry
	 * @throws URISyntaxException error thrown when incorrect syntax is sent to server
	 */
	@PutMapping("/queue/{composite}")
	ResponseEntity<?> replaceQueueEntry(@RequestBody Queue newQueueEntry, @PathVariable("composite") String composite) throws URISyntaxException {

		Queue updatedUser = repository.findById(composite)
				.map(user -> {
					user.setComposite(newQueueEntry.getComposite());
					return repository.save(user);
				})
				.orElseGet(() -> {
					newQueueEntry.setComposite(composite);
					return repository.save(newQueueEntry);
				});

			Resource<Queue> resource = assembler.toResource(updatedUser);

			return ResponseEntity
				.created(new URI(resource.getId().expand().getHref()))
				.body(resource);
	}

	/**
	 * Deletes the specified entry from the Queue table
	 * @param composite entry to delete, with primary key "composite"
	 * @return empty json
	 */
	@DeleteMapping("/queue/{composite}")
	ResponseEntity<?> deleteQueueEntry(@PathVariable("composite") String composite) {
		repository.deleteById(composite);

		return ResponseEntity.noContent().build();
	}
}