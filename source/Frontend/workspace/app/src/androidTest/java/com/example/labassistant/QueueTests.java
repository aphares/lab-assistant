package com.example.labassistant;

import android.support.test.espresso.ViewInteraction;
import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.example.labassistant.app.Activities.MainActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.longClick;
import static android.support.test.espresso.assertion.ViewAssertions.doesNotExist;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.example.labassistant.EspressoTools.EspressoTestsMatchers.getCountFromList;
import static com.example.labassistant.EspressoTools.EspressoTestsMatchers.withDrawable;
import static com.example.labassistant.EspressoTools.EspressoTestsMatchers.withListSize;
import static junit.framework.TestCase.assertEquals;
import static org.hamcrest.CoreMatchers.anything;
import static org.hamcrest.core.IsNot.not;

@RunWith(AndroidJUnit4.class)
public class QueueTests {

    ViewInteraction listViewInteraction;
    ViewInteraction addBtn;
    ViewInteraction removeBtn;
    ViewInteraction popupCancelBtn;
    ViewInteraction popupCheckoffBtn;
    ViewInteraction popupQuestionBtn;
    String firstUserName;
    String testUserName;

    @Rule
    public ActivityTestRule<MainActivity> activityActivityTestRule =
            new ActivityTestRule<>(MainActivity.class);


    @Before
    public void initSetup(){
        onView(withId(R.id.navigationQueue)).perform(click());

        listViewInteraction = onView(withId(R.id.queueListView));
        addBtn = onView(withId(R.id.queueAddBtn));
        removeBtn = onView(withId(R.id.queueRemoveBtn));
        popupCheckoffBtn = onView(withId(R.id.checkoffBtn));
        popupQuestionBtn = onView((withId(R.id.questionBtn)));
        popupCancelBtn = onView(withId(R.id.queueAddCancelTextView));
        firstUserName = "Dresse, Anna-maria";
        testUserName = "Astudent, Student";
    }

    @Test
    public void queueViewVisibility(){
        onView(withId(R.id.navigationQueue)).check(matches((isDisplayed())));
        onView(withId(R.id.queueAddBtn)).check(matches((isDisplayed())));
    }

    @Test
    public void queueListSize(){
        onView(withId(R.id.queueListView)).check(ViewAssertions.matches(withListSize(3)));
    }
//
//    @Test
//    public void queueFirstEntry(){
//        onData(anything()).inAdapterView(withId(R.id.queueListView)).onChildView(
//                withId(R.id.queueIndividualNameTextView)).atPosition(0).check(
//                matches(withText(firstUserName)));
//
//        onData(anything()).inAdapterView(withId(R.id.queueListView)).onChildView(
//                withId(R.id.queueIndividualReasonImageView)).atPosition(0).check(
//                matches(withDrawable(R.drawable.ic_check_black_24dp)));
//    }

    @Test
    public void queueAddPopupIsDisplayed(){
        addBtn.perform(click());
        onView(withId(R.id.chatDeletePopupTitleTextView)).check(matches(isDisplayed()));
    }

    @Test
    public void queueAddPopupCancel(){
        queueAddPopupIsDisplayed();
        popupCancelBtn.perform(click());
        onView(withId(R.id.chatDeletePopupTitleTextView)).check(doesNotExist());
    }

    @Test
    public void queueAddUserWithCheckpointReason() throws InterruptedException {
        // Get count of current list
        int initialSize = getCountFromList(R.id.queueListView);

        // Add user with reason checkoff
        queueAddPopupIsDisplayed();
        popupCheckoffBtn.perform(click());
        onView(withId(R.id.chatDeletePopupTitleTextView)).check(doesNotExist());

        // Wait for websocket
        Thread.sleep(2000);

        // Checks to see if this user was added to the queue with the right name
        // Note that initialSize is used because list starts at 0
        onData(anything()).inAdapterView(withId(R.id.queueListView)).onChildView(
                withId(R.id.queueIndividualNameTextView)).atPosition(initialSize).check(
                matches(withText(testUserName)));

        // Checks to see if this user was added to the queue with the right reason
        // Note that initialSize is used because list starts at 0
        onData(anything()).inAdapterView(withId(R.id.queueListView)).onChildView(
                withId(R.id.queueIndividualReasonImageView)).atPosition(initialSize).check(
                matches(withDrawable(R.drawable.ic_check_black_24dp)));

        // Holds the item down to display delete button
        onData(anything()).inAdapterView(withId(R.id.queueListView)).onChildView(
                withId(R.id.queueIndividualReasonImageView)).atPosition(initialSize).perform(longClick());

        //Presses the delete button
        removeBtn.perform(click());

        // Wait for websocket
        Thread.sleep(2000);
    }

    @Test
    public void queueAddUserWithQuestionReason() throws InterruptedException {
        // Get count of current list
        int initialSize = getCountFromList(R.id.queueListView);

        // Add user with reason question
        queueAddPopupIsDisplayed();
        popupQuestionBtn.perform(click());
        onView(withId(R.id.chatDeletePopupTitleTextView)).check(doesNotExist());

        // Wait for websocket
        Thread.sleep(2000);

        // Checks to see if this user was added to the queue with the right name
        // Note that initialSize is used because list starts at 0
        onData(anything()).inAdapterView(withId(R.id.queueListView)).onChildView(
                withId(R.id.queueIndividualNameTextView)).atPosition(initialSize).check(
                matches(withText(testUserName)));

        // Checks to see if this user was added to the queue with the right reason
        // Note that initialSize is used because list starts at 0
        onData(anything()).inAdapterView(withId(R.id.queueListView)).onChildView(
                withId(R.id.queueIndividualReasonImageView)).atPosition(initialSize).check(
                matches(withDrawable(R.drawable.ic_question_mark)));
    }

    @Test
    public void queueAddUserWithTwoDifferentReasonsTest() throws InterruptedException {

        // Get count of current list
        int initialSize = getCountFromList(R.id.queueListView);

        // Add user with reason checkoff
        queueAddPopupIsDisplayed();
        popupCheckoffBtn.perform(click());
        onView(withId(R.id.chatDeletePopupTitleTextView)).check(doesNotExist());

        // Wait for websocket
        Thread.sleep(2000);

        // Get new size after add
        int sizeAfterFirstAdd = getCountFromList(R.id.queueListView);

        // Add user with reason question
        queueAddPopupIsDisplayed();
        popupQuestionBtn.perform(click());
        onView(withId(R.id.chatDeletePopupTitleTextView)).check(doesNotExist());

        // Wait for websocket
        Thread.sleep(2000);

        // Get new size after add
        int sizeAfterSecondAdd = getCountFromList(R.id.queueListView);

        // Check sizes
        assertEquals(initialSize + 1, sizeAfterFirstAdd);
        assertEquals(initialSize + 2, sizeAfterSecondAdd);
    }

    @Test
    public void queueAddFull() throws InterruptedException {
        queueAddUserWithTwoDifferentReasonsTest();

        // Attempts to add when queue is filled for this user, thus giving the user a warning
        addBtn.perform(click());
        onView(withId(R.id.chatDeletePopupTitleTextView)).check(doesNotExist());
    }

    @Test
    public void queueAddDuplicateCheckpointReason() throws InterruptedException {
        queueAddUserWithCheckpointReason();

        // Add user with duplicate reason checkoff
        queueAddPopupIsDisplayed();
        popupCheckoffBtn.perform(click());

        // Add popup should still be visible after the attempt
        onView(withId(R.id.chatDeletePopupTitleTextView)).check(matches(isDisplayed()));
    }

    @Test
    public void queueAddDuplicateQuestionReason() throws InterruptedException {
        queueAddUserWithQuestionReason();

        // Add user with duplicate reason question
        queueAddPopupIsDisplayed();
        popupQuestionBtn.perform(click());

        // Add popup should still be visible after the attempt
        onView(withId(R.id.chatDeletePopupTitleTextView)).check(matches(isDisplayed()));
    }



}
