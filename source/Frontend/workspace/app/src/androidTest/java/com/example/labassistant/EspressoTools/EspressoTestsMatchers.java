package com.example.labassistant.EspressoTools;

import android.support.annotation.IdRes;
import android.view.View;
import android.widget.ListView;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.withId;


public class EspressoTestsMatchers {

    private static int count;

    //Credit goes to Daniele Bottillo for coming up with a Espresso's Drawable Matcher
    public static Matcher<View> withDrawable(final int resourceId) {
        return new DrawableMatcher(resourceId);
    }

    //Credit goes to Daniele Bottillo for coming up with a Espresso's Drawable Matcher
    public static Matcher<View> noDrawable() {
        return new DrawableMatcher(-1);
    }

    //Credit goes to Cory Roy
    public static Matcher<View> withListSize (final int size) {
        return new TypeSafeMatcher<View>() {
            @Override public boolean matchesSafely (final View view) {
                return ((ListView) view).getCount () == size;
            }

            @Override public void describeTo (final Description description) {
                description.appendText ("ListView should have " + size + " items");
            }
        };
    }

    //Credit goes to Jing Li, this return the size of a list adapter in Expresso
    public static int getCountFromList(@IdRes int listViewId) {
        count = 0;

        Matcher matcher = new TypeSafeMatcher<View>() {
            @Override
            protected boolean matchesSafely(View item) {
                count = ((ListView) item).getCount();
                return true;
            }

            @Override
            public void describeTo(Description description) {
            }
        };

        onView(withId(listViewId)).check(matches(matcher));

        int result = count;
        count = 0;
        return result;
    }
}
