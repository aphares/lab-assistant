package com.example.labassistant.app.Settings;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.example.labassistant.R;
import com.example.labassistant.app.Activities.LoginActivity;
import com.example.labassistant.app.Settings.Classroom.SettingsClassroomFragment;


/**
 * A simple {@link Fragment} subclass.
 *
 * @author Marco Yepez
 * @author Joe Herrera
 */
public class SettingsFragment extends Fragment {

    private LinearLayout classroomLinearLayoutObject;
    SettingsClassroomFragment settingsClassroomFragment;

    Button logoutButton;

    int permission;

    /**
     * Constructor for SettingsFragment
     */
    public SettingsFragment() {
        // Required empty public constructor
    }

    /**
     * @param inflater
     *      Given inflater
     * @param container
     *      Given container
     * @param savedInstanceState
     *      Given savedInstanceState
     * @return
     *      Inflater for SettingFragment
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View inflate = inflater.inflate(R.layout.fragment_settings, container, false);
        classroomLinearLayoutObject = (LinearLayout) inflate.findViewById(R.id.classroomLinearLayout);
        View classroomdivider = (View) inflate.findViewById(R.id.classroomDivider);
        settingsClassroomFragment = new SettingsClassroomFragment();
        logoutButton = (Button) inflate.findViewById(R.id.logoutBtn);

        classroomLinearLayoutObject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setFragment(settingsClassroomFragment);
            }
        });

        // On click listener for the logout button
        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(getActivity(), LoginActivity.class);
                getActivity().startActivity(myIntent);
            }
        });

        // Implement views according permissions
        SharedPreferences sharedPreferences = getContext().getSharedPreferences("userInfo",
                Context.MODE_PRIVATE);
        permission = sharedPreferences.getInt("permission", 2);

        if(permission == 1){
            // User has professor permissions
            // Do nothing
        }
        else if(permission == 2){
            // User has ta permissions
            // Do nothing
        }
        else if(permission == 3){
            // User as student permissions
            // Remove 'Classroom' button and hide extra divider
            classroomLinearLayoutObject.setVisibility(View.GONE);
            classroomdivider.setVisibility(View.GONE);
        }


        // Inflate the layout for this fragment
        return inflate;
    }

    /**
     * Sets the fragment
     * @param frag
     *      fragment
     */
    private void setFragment(Fragment frag){
        FragmentTransaction fragmentTransaction =
                getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.mainFrame, frag);
        fragmentTransaction.commit();
    }
}
