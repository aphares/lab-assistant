package com.example.labassistant.app.Settings.Classroom;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.labassistant.R;
import com.example.labassistant.utilities.CustomSingleton;


/**
 * A simple {@link Fragment} subclass.
 *
 * @author Marco Yepez
 * @author Joe Herrera
 */
public class SettingsClassroomAdd extends Fragment {

    private String addingStudentDirectory = "http://cs309-rr-2.misc.iastate.edu:8080/students";
    private String addingTADirectory = "http://cs309-rr-2.misc.iastate.edu:8080/tas";

    /**
     * Constructor for SettingsClassroomAdd
     */
    public SettingsClassroomAdd() {
        // Required empty public constructor
    }

    /**
     * onCreateView method
     *
     * @param inflater
     *      inflater
     * @param container
     *      container
     * @param savedInstanceState
     *      saved instance state
     * @return
     *      inflate
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        final View inflate =  inflater.inflate(R.layout.fragment_settings_classroom_add, container, false);

        return inflate;
    }

    /**
     * Adds a user in backend
     *
     * @param classroomIndividual
     *      classroom individual being added
     */
    public void addUser(ClassroomIndividual classroomIndividual){
        String directoryToAdd = "";
        if(classroomIndividual.getStudentOrTA().equals("ta".toLowerCase())){
            directoryToAdd = addingTADirectory + "/" + classroomIndividual.getUsername();
        }
        else if(classroomIndividual.getStudentOrTA().equals("student".toLowerCase())){
            directoryToAdd = addingTADirectory + "/" + classroomIndividual.getUsername();
        }


        StringRequest stringRequest = new StringRequest(Request.Method.POST, directoryToAdd,
                new Response.Listener<String>() {
                    @SuppressLint("RestrictedApi")
                    @Override
                    public void onResponse(String response) {
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast toast = Toast.makeText(getContext(), "Failed add selected user",
                        Toast.LENGTH_LONG);
                toast.show();
                error.printStackTrace();
            }
        });
        CustomSingleton.getInstance(getContext()).addToRequestQueue(stringRequest);
    }
}
