package com.example.labassistant.app.Queue;

/**
 * Queue Individual objects used to help list adapter and fragment
 *
 * @author Marco Yepez
 * @author Joe Herrera
 */
public class QueueIndividual {
    private String firstName, lastName, reason, username;
    private boolean selected;

    /**
     * Construct for class
     *
     * @param firstName
     *      first name
     * @param lastName
     *      last name
     * @param reason
     *      reason
     * @param username
     *      username
     */
    public QueueIndividual(String firstName, String lastName, String reason, String username){
        this.firstName = firstName;
        this.lastName = lastName;
        this.reason = reason;
        this.username = username;
        this.selected = false;
    }

    /**
     * Gets the first name involved in the Queue Individual object
     *
     * @return
     *      firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the first name involved in the Queue Individual object
     *
     * @param firstName
     *      firstName
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Gets the last name involved in the Queue Individual object
     *
     * @return
     *      lasttName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the last name involved in the Queue Individual object
     *
     * @param lastName
     *      last name
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Gets the reason involved in the Queue Individual object
     *
     * @return
     *      reason
     */
    public String getReason() {
        return reason;
    }

    /**
     * Sets the reason involved in the Queue Individual object
     *
     * @param reason
     *      reason
     *
     */
    public void setReason(String reason) {
        this.reason = reason;
    }

    /**
     * Gets the full name involved in the Queue Individual object
     *
     * @return
     *      full name
     */
    public String getFullName(){
        return lastName + ", " + firstName;
    }

    /**
     * Gets the selected involved in the Queue Individual object
     * @return
     *      selected
     */
    public boolean isSelected() {
        return selected;
    }

    /**
     * Sets the selected involved in the Queue Individual object
     *
     * @param selected
     *      select
     */
    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    /**
     * Gets the username involved in the Queue Individual object
     *
     * @return
     *      username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets the username involved in the Queue Individual object
     *
     * @param username
     *      username
     */
    public void setUsername(String username) {
        this.username = username;
    }
}
