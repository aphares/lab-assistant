package com.example.labassistant.utilities;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.labassistant.R;
import com.example.labassistant.app.Classwork.Classwork;

import java.util.ArrayList;
import java.util.List;

/**
 * List adapter for classwork
 * @author Marco Yepez
 * @author Joe Herrera
 */
public class ClassworkListAdapter extends BaseAdapter {
    private List<Classwork> classworkList = new ArrayList<>();
    private Context context;

    /**
     * Constructor for ClassworkListAdapter
     *
     * @param givenContext
     *      context
     */
    public ClassworkListAdapter(Context givenContext){
        context = givenContext;
    }

    /**
     * Adds classwork to list
     * @param classwork
     *      classwork
     */
    public void addClasswork(Classwork classwork){
        classworkList.add(classwork);
    }

    /**
     * Gets count of list
     * @return
     *      count
     */
    @Override
    public int getCount() {
        return classworkList.size();
    }

    /**
     * Gets item from list
     * @param position
     *      position in the list
     * @return
     *      item
     */
    @Override
    public Object getItem(int position) {
        return classworkList.get(position);
    }

    /**
     * id for item
     * @param position
     *      position in the list
     * @return
     *      id
     */
    @Override
    public long getItemId(int position) {
        return 0;
    }

    /**
     * Creates view for this list view
     *
     * @param position
     *      position in the list
     * @param convertView
     *      view
     * @param parent
     *      parent
     * @return
     *      converted view
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Classwork currentClasswork = (Classwork) getItem(position);
        final LayoutInflater classworkInflator =
                (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        convertView = classworkInflator.inflate(R.layout.classwork_list_layout, null);

        TextView classworkTitleTextView =
                convertView.findViewById(R.id.classworkTitleTextView);
        TextView classworkCheckpoint01TextView =
                convertView.findViewById(R.id.classworkCheckpoint01TextView);
        TextView classworkCheckpoint02TextView =
                convertView.findViewById(R.id.classworkCheckpoint02TextView);

        classworkTitleTextView.setText(currentClasswork.getTitle());
        classworkCheckpoint01TextView.setText(currentClasswork.getCheckpoint01());
        classworkCheckpoint02TextView.setText(currentClasswork.getCheckpoint02());

        return convertView;
    }
}
