package com.example.labassistant.app.Settings.Classroom;


import android.annotation.SuppressLint;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.labassistant.R;
import com.example.labassistant.app.Activities.MainActivity;
import com.example.labassistant.utilities.PopUpUtils;


/**
 * A simple {@link Fragment} subclass.
 *
 * @author Marco Yepez
 * @author Joe Herrera
 */
public class SettingsClassroomFragment extends Fragment {

    SettingsClassroomStudentFragment settingsClassroomStudentFragment;
    SettingsClassroomTaFragment settingsClassroomTaFragment;

    //Init variables needed throughout the class
    private ImageView sortView;
    private EditText searchBarEditText;



    TabLayout tabLayout;

    // 0 for student tab
    // 1 for ta tab
    public static int currentTab;

    /**
     * Constructor for SettingsClassroomFragment
     */
    public SettingsClassroomFragment() {
        // Required empty public constructor
    }

    /**
     * onCreateView method
     *
     * @param inflater
     *      inflater
     * @param container
     *      container
     * @param savedInstanceState
     *      saved instance state
     * @return
     *      inflater
     */
    @SuppressLint("ClickableViewAccessibility")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View inflate = inflater.inflate(R.layout.fragment_settings_classroom, container,
                false);
        settingsClassroomStudentFragment = new SettingsClassroomStudentFragment();
        settingsClassroomTaFragment = new SettingsClassroomTaFragment();

        tabLayout = inflate.findViewById(R.id.classroomTabLayout);

        // Handles the search operations
        searchBarEditText = (EditText) inflate.findViewById(R.id.classroomSearchEditText);

        //Sets the current tab to student
        currentTab = 0;
        
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0){
                    setFragment(settingsClassroomStudentFragment);
                    currentTab = 0;
//                    searchBarEditText.setText("");
                }
                else if (tab.getPosition() == 1){
                    setFragment(settingsClassroomTaFragment);
                    currentTab = 1;
//                    searchBarEditText.setText("");
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        setFragment(settingsClassroomStudentFragment);


        //Handles the sort button click
        sortView = (ImageView) inflate.findViewById(R.id.sortImageView);
        sortView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopUpUtils.generateSortPopup(inflate, getLayoutInflater());
            }
        });


        // Drawable search and x views
        final Drawable xDrawable = inflate.getResources().getDrawable(R.drawable.ic_clear_black_24dp);
        xDrawable.setBounds(0, 0, xDrawable.getIntrinsicWidth(), xDrawable.getIntrinsicHeight());
        final Drawable searchDrawable = inflate.getResources().getDrawable(R.drawable.ic_search_black_24dp);
        searchDrawable.setBounds(0, 0, searchDrawable.getIntrinsicWidth(), searchDrawable.getIntrinsicHeight());

        // On Touch listener for the clear button on the search bar
        searchBarEditText.setOnTouchListener(new View.OnTouchListener() {
            final int DRAWABLE_LEFT = 0;
            final int DRAWABLE_TOP = 1;
            final int DRAWABLE_RIGHT = 2;
            final int DRAWABLE_BOTTOM = 3;
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                //Checks if the right drawable exists
                Drawable rightDrawable = searchBarEditText.getCompoundDrawables()[2];
                if (rightDrawable == null){
                    return false;
                }
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    int leftEdgeOfRightDrawable = searchBarEditText.getRight()
                            - searchBarEditText.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width();
                    if (event.getRawX() >= leftEdgeOfRightDrawable) {
                        // clicked on clear icon
                        searchBarEditText.setText("");
                        return true;
                    }
                }
                return false;
            }
        });

        // Text change listener for search bar
        searchBarEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String searchString = s.toString().trim();
                if (s.length() == 0) {
                    searchBarEditText.setCompoundDrawables(searchDrawable, null, null, null);

                    // Calls to get all users in the current tab
                    // Student
                    if (currentTab == 0){
                            SettingsClassroomStudentFragment.search(inflate, searchString);
                    }
                    // TA
                    else{
                        SettingsClassroomTaFragment.search(inflate, searchString);
                    }

                } else if (s.charAt(s.length() - 1) == '\n') {
                    searchBarEditText.setCompoundDrawables(searchDrawable, null, xDrawable, null);
                    searchBarEditText.setText(searchString);

                    // Hides the keyboard when enter is pressed
                    MainActivity.hideKeyboardFrom(inflate.getContext(), searchBarEditText);
                    searchBarEditText.clearFocus();
                    searchBarEditText.setSelection(searchBarEditText.getText().length());
                } else {
                    searchBarEditText.setCompoundDrawables(searchDrawable, null, xDrawable, null);

                    // Student
                    if (currentTab == 0){
                        SettingsClassroomStudentFragment.search(inflate, searchString);
                    }
                    // TA
                    else{
                        SettingsClassroomTaFragment.search(inflate, searchString);
                    }
                }
            }
        });


        return inflate;
    }

    /**
     * Sets the next fragment to be placed
     *
     * @param frag
     *      fragment to be placed
     */
    private void setFragment(Fragment frag){
        FragmentTransaction fragmentTransaction =
                getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.classroomListFragment, frag);
        fragmentTransaction.commit();
    }

}
