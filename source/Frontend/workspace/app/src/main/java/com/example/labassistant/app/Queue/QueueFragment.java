package com.example.labassistant.app.Queue;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.labassistant.R;
import com.example.labassistant.utilities.CustomSingleton;
import com.example.labassistant.utilities.QueueListAdapter;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.drafts.Draft;
import org.java_websocket.drafts.Draft_6455;
import org.java_websocket.handshake.ServerHandshake;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 *
 * @author Marco Yepez
 * @author Joe Herrera
 */
public class QueueFragment extends Fragment {
    String queueUrl = "http://cs309-rr-2.misc.iastate.edu:8080/queue";

//    String websocketUrl = "ws://cs309-rr-2.misc.iastate.edu:8080/queue";
    String websocketUrl = "ws://cs309-rr-2.misc.iastate.edu:8080/LAWebsockets/";

    private static WebSocketClient webSocketClient;

    String currentUser;
    int permission;

    private QueueIndividual queueIndividual;
    private QueueListAdapter queueListAdapter;

    private ListView queueListView;
    private FloatingActionButton addBtn;
    private FloatingActionButton removeBtn;

    private int selectedUser;

    /**
     * Constructor for QueueFragment class
     */
    public QueueFragment() {
        // Required empty public constructor
    }


    /**
     * Method called when object is created
     *
     * @param inflater
     *      Given inflater
     * @param container
     *      Given container
     * @param savedInstanceState
     *      Given savedInstanceState
     * @return
     *      Inflater for QueueFragment
     */
    @SuppressLint("RestrictedApi")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View inflate = inflater.inflate(R.layout.fragment_queue, container, false);

        SharedPreferences sharedPreferences = getContext().getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        currentUser = sharedPreferences.getString("username", "student");
        permission = sharedPreferences.getInt("permission", 3);
        selectedUser = -1;

        addBtn = (FloatingActionButton) inflate.findViewById(R.id.queueAddBtn);
        removeBtn = (FloatingActionButton) inflate.findViewById(R.id.queueRemoveBtn);

        queueListAdapter = new QueueListAdapter(getContext());
        queueListView = inflate.findViewById(R.id.queueListView);

        // Creates a websocket with the server
//        queueListView.setAdapter(queueListAdapter);
        websocketUrl += currentUser + "/";
        startWebsocket();

//        getAllQueueList();

        // Set OnClick Listener for the add button in queue
        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<String> currentReasons = new ArrayList<>();

                // Checks to see if the user already exist in the list adaptor and for what reasons
                // Searches to see if the user is already in the list adapter
                for (int i = 0; i < queueListAdapter.getCount(); i++){
                    QueueIndividual queueIndividual = (QueueIndividual) queueListAdapter.getItem(i);

                    // Checks if the usernames are equal
                    if(queueIndividual.getUsername().compareTo(currentUser) == 0){
                        // Checks what reason the student previously had
                        if (queueIndividual.getReason().compareTo("checkoff") == 0){
                            currentReasons.add("checkoff");
                        }
                        if(queueIndividual.getReason().compareTo("question") == 0){
                            currentReasons.add("question");
                        }
                    }
                }

                if (currentReasons.size() == 2){
                    Toast.makeText(inflate.getContext(), "An user can only queue twice", Toast.LENGTH_LONG).show();
                }
                else {
//                    webSocketClient.send("queue");
//                    webSocketClient.send("classroom");

//                    webSocketClient.closeConnection(0, "Closing connection");
                    QueueAddPopup.generateQueueAddPopup(inflate, queueListAdapter, currentReasons, webSocketClient);
                }
            }
        });

        // Set OnClick Listener on queue list to correctly select an item
        queueListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @SuppressLint("RestrictedApi")
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                QueueIndividual queueIndividual =
                        (QueueIndividual) queueListAdapter.getItem(position);
                if(!queueListAdapter.isOneSelectedFromQueueList() &&
                        queueIndividual.getUsername().equals(currentUser)){
                    queueIndividual.setSelected(true);
                    selectedUser = position;
                    removeBtn.setVisibility(View.VISIBLE);
                    addBtn.setVisibility(View.GONE);
                    queueListAdapter.notifyDataSetChanged();
                }
                else if(!queueListAdapter.isOneSelectedFromQueueList() && permission == 1 ||
                permission == 2){
                    queueIndividual.setSelected(true);
                    selectedUser = position;
                    removeBtn.setVisibility(View.VISIBLE);
                    addBtn.setVisibility(View.GONE);
                    queueListAdapter.notifyDataSetChanged();
                }
                return true;
            }
        });

        // Set OnClick Listener on the whole queue list so if pressed again after selecting one it
        // can deselected selected users
        queueListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(queueListAdapter.isOneSelectedFromQueueList()){
                    queueListAdapter.deselectIndividuals();
                    selectedUser = -1;
                    removeBtn.setVisibility(View.GONE);
                    if(permission == 3){
                        addBtn.setVisibility(View.VISIBLE);
                    }
                    queueListAdapter.notifyDataSetChanged();
                }
            }
        });

        // Set OnClick Listener on remove button to remove selected user
        removeBtn.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("RestrictedApi")
            @Override
            public void onClick(View v) {
                removeFromQueue(selectedUser);
                removeBtn.setVisibility(View.GONE);
                if(permission == 3){
                    addBtn.setVisibility(View.VISIBLE);
                }
            }
        });

        // Displays correct display depending on permissions given
        if(permission == 1 || permission == 2){
            addBtn.setVisibility(View.GONE);
        }


        return inflate;
    }

    /**
     * Get the complete list of the queue from backend and displays it on the screen using the
     * list adapter
     */
    private void getAllQueueList(){
        // The following is used to demonstrate the view of the ListView to the developer.
        // This can be deleted or commented out
//        queueIndividual = new QueueIndividual("Joe", "Herrera",
//                "question", "jherrera");
//        queueListAdapter.addQueueIndividual(queueIndividual);
//        queueIndividual = new QueueIndividual("Marco", "Yepez",
//                "checkoff", "myepez");
//        queueListAdapter.addQueueIndividual(queueIndividual);
//        queueIndividual = new QueueIndividual("Lorenzo", "Chavarria",
//                "checkoff", "lchavarria");
//        queueListAdapter.addQueueIndividual(queueIndividual);
//        queueIndividual = new QueueIndividual("Nicolas", "De La Cruz",
//                "question", "ndelacurz");
//        queueListAdapter.addQueueIndividual(queueIndividual);
//        queueListView.setAdapter(queueListAdapter);

        // TODO Complete the queue to get all of queue list from backend when Andrew is done
//        websocket.setSendMessage("queue");
//        webSocketClient.send("queue");




//        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
//                queueUrl, null, new Response.Listener<JSONObject>() {
//            @Override
//            public void onResponse(JSONObject response) {
//                try {
//                    String embedded_obj = response.getString("_embedded");
//                    JSONObject jsonObject = new JSONObject(embedded_obj);
//                    JSONArray users_table = jsonObject.getJSONArray("queueList");
//
//                    int index = 0;
//                    while(index < users_table.length()){
//                        JSONObject currentObject = users_table.getJSONObject(index);
//                        JSONObject studentObject = currentObject.getJSONObject("student");
//                        JSONObject userObject = studentObject.getJSONObject("user");
//
//                        String firstName = userObject.getString("firstname");
//                        String lastName = userObject.getString("lastname");
//                        String username = userObject.getString("username");
//                        String reason = currentObject.getString("reason");
//
//                        queueIndividual = new QueueIndividual(firstName, lastName, reason, username);
//                        queueListAdapter.addQueueIndividual(queueIndividual);
//                        index++;
//                    }
//                    queueListView.setAdapter(queueListAdapter);
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Toast toast = Toast.makeText(getContext(), "Failed to get queue list",
//                        Toast.LENGTH_LONG);
//                toast.show();
//                error.printStackTrace();
//            }
//        });
//        CustomSingleton.getInstance(getContext()).addToRequestQueue(jsonObjectRequest);
    }

    /**
     * Removes the user from the list adapter and updates the view
     * @param position
     *      position of item that needs to be removed from list adapter
     */
    public void removeFromQueue(int position){
        selectedUser = -1;

        queueIndividual = (QueueIndividual) queueListView.getAdapter().getItem(position);

        // Removing using websockets


        webSocketClient.send("DelQueue:" + queueIndividual.getUsername() + "," + queueIndividual.getReason());
//        queueListAdapter.removeItem(position);
//        queueListAdapter.notifyDataSetChanged();


//        queueListAdapter.clearList();

//        webSocketClient.send("queue");
//        getAllQueueList();


//        final String deleteURL = queueUrl + "/" + queueIndividual.getUsername();
//        StringRequest stringRequest = new StringRequest(Request.Method.DELETE, deleteURL,
//                new Response.Listener<String>() {
//                    @SuppressLint("RestrictedApi")
//                    @Override
//                    public void onResponse(String response) {
//                        queueListAdapter.clearList();
//                        getAllQueueList();
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Toast toast = Toast.makeText(getContext(), "Failed delete selected student",
//                        Toast.LENGTH_LONG);
//                toast.show();
//                error.printStackTrace();
//            }
//        });
//        CustomSingleton.getInstance(getContext()).addToRequestQueue(stringRequest);
    }

    /**
     * Gets the list adapter for this queue
     *
     * @return
     *      list adapter
     */
    public QueueListAdapter getListAdapter(){
        return queueListAdapter;
    }

    /**
     * Method updates the list
     *
     * @param queueListAdapter
     *      list adapter
     */
    private void updateList(QueueListAdapter queueListAdapter){
        queueListView.setAdapter(queueListAdapter);
    }

    /**
     * Starts websocket
     */
    public void startWebsocket(){
        try{
            Draft[] drafts = {new Draft_6455()};
            webSocketClient = new WebSocketClient(new URI(websocketUrl), drafts[0]) {
                @Override
                public void onOpen(ServerHandshake serverHandshake) {
                    Log.d("OPEN", "Connection Opened");
                    webSocketClient.send("queue");
                }

                @Override
                public void onMessage(String s) {
                    Log.d("RECEIVED", s);
                    String receivedString = s;

                    // Init connection
                    if(receivedString.contains("connected")){
                        return;
                    }

                    // Parsing the string

                    try {
                        queueListAdapter.clearList();
                        JSONArray recievedJsonArray = new JSONArray(s);
                        int index = 0;
                        while(index < recievedJsonArray.length()){
                            JSONObject currentJsonObject = recievedJsonArray.getJSONObject(index);
                            JSONObject contentObject = currentJsonObject.getJSONObject("content");
                            String reason = contentObject.getString("reason");
                            String username = contentObject.getString("username");
                            JSONObject studentObject = contentObject.getJSONObject("student");
                            String firstName = studentObject.getString("firstname");
                            String lastName = studentObject.getString("lastname");
                            QueueIndividual queueIndividual =
                                    new QueueIndividual(firstName, lastName, reason, username);
                            queueListAdapter.addQueueIndividual(queueIndividual);
                            index++;
                        }
                        queueListView.post(new Runnable() {
                            @Override
                            public void run() {
                                queueListView.setAdapter(queueListAdapter);
                            }
                        });
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getContext(), "Something went wrong with the websocket", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onClose(int i, String s, boolean b) {
                    Log.d("CLOSE", "Connection Closed");
                }

                @Override
                public void onError(Exception e) {
                    Log.d("ERROR", "Connection ERROR");
                }
            };
        }
        catch (URISyntaxException e){
            Log.d("Exception: ", e.getMessage().toString());
            e.printStackTrace();
            Toast.makeText(getContext(), "Something went wrong with the websocket", Toast.LENGTH_LONG).show();
        }
        webSocketClient.connect();
    }

    /**
     * Gets the websocket from this class
     *
     * @return
     *      websocket
     */
    public static WebSocketClient getWebSocketClient(){
        return webSocketClient;
    }

}
