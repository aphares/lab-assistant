package com.example.labassistant.app.Settings.Classroom;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.example.labassistant.R;
import com.example.labassistant.utilities.ClassroomListAdapter;

import java.util.Collections;


/**
 * A simple {@link Fragment} subclass.
 *
 * @author Marco Yepez
 * @author Joe Herrera
 */
public class SettingsClassroomSort extends Fragment {

    /**
     * Constructor for SettingsClassroomSort
     */
    public SettingsClassroomSort() {
        // Required empty public constructor
    }

    /**
     * onCreateView method
     *
     * @param inflater
     *      inflater
     * @param container
     *      container
     * @param savedInstanceState
     *      saved instance state
     * @return
     *      inflater
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View inflate = inflater.inflate(R.layout.fragment_settings_classroom_sort, container, false);

        return inflate;
    }

    /**
     * Grabs the previous pref for sorting and calls the corresponding sort method
     * @param view
     *      view
     */
    public static void sortByDefault(View view){

        //Checking last button state and highlighting the corresponding radio button
        SharedPreferences sharedPreferences = view.getContext().getSharedPreferences("preferences", Context.MODE_PRIVATE);
        int lastButtonState = sharedPreferences.getInt("RADIOBUTTON", 2);

        switch (lastButtonState){
            case 0:
                sortByAscendingAlpha(view);
                break;
            case 1:
                sortByDescendingAlpha(view);
                break;
            case 2:
                sortByAscendingSection(view);
                break;
            case 3:
                sortByDescendingSection(view);
                break;
        }
    }

    /**
     * Rearranges the ClassroomListAdapter by Descending Section number
     * @param view
     *      given view
     */
    public static void sortByDescendingSection(View view){

        ListView currentListView;

        if (SettingsClassroomFragment.currentTab == 0){

            currentListView = (ListView) view.findViewById(R.id.classroomStudentListView);
        }
        else{
            currentListView = (ListView) view.findViewById(R.id.classroomTAListView);
        }
        ClassroomListAdapter classroomListAdapter = (ClassroomListAdapter) currentListView.getAdapter();
        Collections.sort(classroomListAdapter.getArrayList(), ClassroomIndividual.individualSectionDescendingComparator);
        classroomListAdapter.notifyDataSetChanged();
    }

    /**
     * Rearranges the ClassroomListAdapter by Ascending Section number
     * @param view
     *      given view
     */
    public static void sortByAscendingSection(View view){

        ListView currentListView;

        if (SettingsClassroomFragment.currentTab == 0){

            currentListView = (ListView) view.findViewById(R.id.classroomStudentListView);
        }
        else{
            currentListView = (ListView) view.findViewById(R.id.classroomTAListView);
        }
        ClassroomListAdapter classroomListAdapter = (ClassroomListAdapter) currentListView.getAdapter();
        Collections.sort(classroomListAdapter.getArrayList(), ClassroomIndividual.individualSectionAscendingComparator);
        classroomListAdapter.notifyDataSetChanged();
    }

    /**
     * Rearranges the ClassroomListAdapter by Ascending Alphabetical order
     * @param view
     *      given view
     */
    public static void sortByAscendingAlpha(View view){
        ListView currentListView;

        if (SettingsClassroomFragment.currentTab == 0){

            currentListView = (ListView) view.findViewById(R.id.classroomStudentListView);
        }
        else{
            currentListView = (ListView) view.findViewById(R.id.classroomTAListView);
        }
        ClassroomListAdapter classroomListAdapter = (ClassroomListAdapter) currentListView.getAdapter();
        Collections.sort(classroomListAdapter.getArrayList(), ClassroomIndividual.individualAlphaAscendingComparator);
        classroomListAdapter.notifyDataSetChanged();
    }


    /**
     * Rearranges the ClassroomListAdapter by Descending Alphabetical order
     * @param view
     *      given view
     */
    public static void sortByDescendingAlpha(View view){
        ListView currentListView;

        if (SettingsClassroomFragment.currentTab == 0){

            currentListView = (ListView) view.findViewById(R.id.classroomStudentListView);
        }
        else{
            currentListView = (ListView) view.findViewById(R.id.classroomTAListView);
        }
        ClassroomListAdapter classroomListAdapter = (ClassroomListAdapter) currentListView.getAdapter();
        Collections.sort(classroomListAdapter.getArrayList(), ClassroomIndividual.individualAlphaDescendingComparator);
        classroomListAdapter.notifyDataSetChanged();
    }
}
