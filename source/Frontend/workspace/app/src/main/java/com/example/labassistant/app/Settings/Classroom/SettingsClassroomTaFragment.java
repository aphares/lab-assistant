package com.example.labassistant.app.Settings.Classroom;


import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.example.labassistant.R;
import com.example.labassistant.utilities.PopUpUtils;
import com.example.labassistant.utilities.ClassroomListAdapter;
import com.example.labassistant.utilities.CustomSingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class SettingsClassroomTaFragment extends Fragment {

    private String taListDirectory = "http://cs309-rr-2.misc.iastate.edu:8080/tas";

    private static ListView classroomTAListView;
    private ClassroomIndividual classroomIndividual;
    private static ClassroomListAdapter classroomListAdapter;
    private static List<ClassroomIndividual> allUsers;



    private static FloatingActionButton addButton;
    private static FloatingActionButton selectAllButton;
    private static FloatingActionButton deleteButton;

    private ImageView sortButton;

    int permission;

    /**
     * Constructor for SettingsClassroomTaFragment
     */
    public SettingsClassroomTaFragment() {
        // Required empty public constructor
    }


    /**
     * onCreateView method
     *
     * @param inflater
     *      inflater
     * @param container
     *      container
     * @param savedInstanceState
     *      saved instance state
     * @return
     *      inflate
     */
    @SuppressLint({"ClickableViewAccessibility", "RestrictedApi"})
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        final View inflate = inflater.inflate(R.layout.fragment_settings_classroom_ta, container,
                false);
        classroomTAListView = inflate.findViewById(R.id.classroomTAListView);

        classroomListAdapter = new ClassroomListAdapter(getContext(), "ta");

        addButton = (FloatingActionButton) inflate.findViewById(R.id.classroomTAAddBtn);
        selectAllButton = inflate.findViewById(R.id.classroomTASelectAllBtn);
        deleteButton = inflate.findViewById(R.id.classroomTADeleteBtn);

        sortButton = getActivity().findViewById(R.id.sortImageView);

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopUpUtils.generateAddPopup(inflate, getLayoutInflater());
            }
        });

        getAllTAs(inflate);

        createListViewOnListenerEvents(inflate);

        // Implement views according permissions
        SharedPreferences sharedPreferences = getContext().getSharedPreferences("userInfo",
                Context.MODE_PRIVATE);
        permission = sharedPreferences.getInt("permission", 2);

        if(permission == 1){
            // User has professor permissions
            // Do nothing
        }
        else if(permission == 2){
            // User has ta permissions
            // Remove add button
            addButton.setVisibility(View.GONE);
        }
        else if(permission == 3){
            // User as student permissions
            // Remove add button
            addButton.setVisibility(View.GONE);
        }

        return inflate;
    }

    /**
     * Listeners for this class are created
     *
     * @param inflate
     *      inflate
     */
    public void createListViewOnListenerEvents(final View inflate){
        sortButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                classroomListAdapter.doClassroomIndividualSetCheckBoxesFalse();
                classroomListAdapter.setShowAllCheckboxes(false);
                removeDeleteView(false);
                classroomListAdapter.setSelectedAll(false);
                classroomListAdapter.notifyDataSetChanged();
                PopUpUtils.generateSortPopup(inflate, getLayoutInflater());
            }
        });

        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<Integer> list = classroomListAdapter.getAllTrueCheckbox();
                for(int i = 0; i < list.size(); i++){
                    deleteATAFromList(list.get(i));
                }
                classroomListAdapter.setShowAllCheckboxes(false);
            }
        });

        selectAllButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(classroomListAdapter.isSelectedAll()){
                    classroomListAdapter.doClassroomIndividualSetCheckBoxesFalse();
                    classroomListAdapter.setShowAllCheckboxes(false);
                    removeDeleteView(false);
                }
                classroomListAdapter.setSelectedAll(!classroomListAdapter.isSelectedAll());
                classroomListAdapter.notifyDataSetChanged();
            }
        });

        classroomTAListView.setOnItemLongClickListener(
                new AdapterView.OnItemLongClickListener() {
                    @SuppressLint("RestrictedApi")
                    @Override
                    public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                                   int pos, long id) {
                        ClassroomIndividual classroomIndividual = (ClassroomIndividual)
                                classroomListAdapter.getItem(pos);
                        classroomIndividual.setCheckBox(true);
                        showDeleteView();
                        return true;
                    }
                });
    }

    /**
     * Retrieves all the tas from backend and displays them
     *
     * @param view
     *      view
     */
    public void getAllTAs(final View view){
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                taListDirectory, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    String embedded_obj = response.getString("_embedded");
                    JSONObject jsonObject = new JSONObject(embedded_obj);
                    JSONArray users_table = jsonObject.getJSONArray("tAList");

                    //Clears the adapter
                    classroomListAdapter = new ClassroomListAdapter(view.getContext());

                    int index = 0;
                    while(index < users_table.length()){
                        JSONObject currentObject = users_table.getJSONObject(index);
                        JSONObject userObject = currentObject.getJSONObject("user");

                        String firstName = userObject.getString("firstname");
                        String lastname = userObject.getString("lastname");
                        String username = userObject.getString("username");
                        String sectionNumber = currentObject.getString("section");
                        int id = userObject.getInt("id");

                        String fullName = lastname + ", " + firstName;

                        classroomIndividual = new ClassroomIndividual(sectionNumber, fullName);
                        classroomIndividual.setUsername(username);
                        classroomIndividual.setId(id);
                        classroomIndividual.setLastname(lastname);
                        classroomListAdapter.addIndividual(classroomIndividual);
                        index++;
                    }
                    classroomTAListView.setAdapter(classroomListAdapter);
                    allUsers = classroomListAdapter.getArrayList();
                    // Sorts the list by default sort
                    SettingsClassroomSort.sortByDefault(getView());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast toast = Toast.makeText(getContext(), "Failed to get TAs",
                        Toast.LENGTH_LONG);
                toast.show();
                error.printStackTrace();
            }
        });
        CustomSingleton.getInstance(getContext()).addToRequestQueue(jsonObjectRequest);
    }

    /**
     * Method implements searching for users
     *
     * @param view
     *      view
     * @param search
     *      search
     */
    public static void search(View view, String search) {

        List<ClassroomIndividual> searchingUsers = new ArrayList<>();
        classroomListAdapter = new ClassroomListAdapter(view.getContext());
        boolean isNumber = false;

        //Checks if the string is an id number or lastname
        if(search.matches(".*\\d+.*")){
            isNumber = true;
        }
        else {
            search = search.toLowerCase();
        }

        if (allUsers == null){
            return;
        }

        for (int i = 0; i < allUsers.size(); i++) {
            ClassroomIndividual classroomIndividual = allUsers.get(i);
            String temp;
            if(isNumber){
                temp = Integer.toString(classroomIndividual.getId());
            }
            else {
                temp = classroomIndividual.getLastname().toLowerCase();
            }

            if (temp.startsWith(search)) {
                classroomListAdapter.addIndividual(classroomIndividual);
            }
        }

        classroomTAListView.setAdapter(classroomListAdapter);

        // Sorts the list by default sort
        SettingsClassroomSort.sortByDefault(view);
    }

    /**
     * Deletes a user from the backend list
     *
     * @param positonInList
     *      position where the user is located in the arraylist
     */
    public void deleteATAFromList(int positonInList) {
        ClassroomIndividual classroomIndividual = (ClassroomIndividual)
                classroomTAListView.getAdapter().getItem(positonInList);

        final String deleteURL = taListDirectory + "/" + classroomIndividual.getUsername();
        StringRequest stringRequest = new StringRequest(Request.Method.DELETE, deleteURL,
                new Response.Listener<String>() {
                    @SuppressLint("RestrictedApi")
                    @Override
                    public void onResponse(String response) {
                        classroomListAdapter.clearList();
                        getAllTAs(getView());
                        removeDeleteView(false);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast toast = Toast.makeText(getContext(), "Failed delete selected TA",
                        Toast.LENGTH_LONG);
                toast.show();
                error.printStackTrace();
            }
        });
        CustomSingleton.getInstance(getContext()).addToRequestQueue(stringRequest);
    }

    /**
     * Removes the delete view
     *
     * @param forceReset
     *      force reset
     */
    @SuppressLint("RestrictedApi")
    public static void removeDeleteView(boolean forceReset){
        addButton.setVisibility(View.VISIBLE);
        selectAllButton.setVisibility(View.INVISIBLE);
        deleteButton.setVisibility((View.INVISIBLE));
    }

    /**
     * Displays the delete view
     */
    @SuppressLint("RestrictedApi")
    public void showDeleteView(){
        addButton.setVisibility(View.INVISIBLE);
        selectAllButton.setVisibility(View.VISIBLE);
        deleteButton.setVisibility((View.VISIBLE));

        classroomListAdapter.setShowAllCheckboxes(true);
        classroomListAdapter.notifyDataSetChanged();
    }

}
