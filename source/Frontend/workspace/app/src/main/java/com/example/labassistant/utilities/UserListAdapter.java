package com.example.labassistant.utilities;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.labassistant.R;
import com.example.labassistant.app.Settings.Classroom.ClassroomIndividual;

import java.util.ArrayList;
import java.util.List;

/**
 * List adapter for user
 * @author Marco Yepez
 * @author Joe Herrera
 */
public class UserListAdapter extends BaseAdapter {
    private List<ClassroomIndividual> allUserIndividuals = new ArrayList<ClassroomIndividual>();
    private List<ClassroomIndividual> searchIndividuals = new ArrayList<ClassroomIndividual>();
    private boolean searching;
    private Context context;

    /**
     * Constructor for UserListAdapter
     * @param givenContext
     *      context
     */
    public UserListAdapter(Context givenContext){
        context = givenContext;
    }

    /**
     * Adds user to list
     * @param givenClassroomIndividual
     */
    public void addIndividual(ClassroomIndividual givenClassroomIndividual){
        allUserIndividuals.add(givenClassroomIndividual);
        notifyDataSetChanged();
    }

    /**
     * Clears list
     */
    public void clearList(){
        allUserIndividuals.clear();
    }

    /**
     * Gets count on  list
     * @return
     *      count
     */
    @Override
    public int getCount() {
        return allUserIndividuals.size();
    }

    /**
     * Gets item on list
     * @param position
     *      position
     * @return
     *      item
     */
    @Override
    public Object getItem(int position) {
        return allUserIndividuals.get(position);
    }

    /**
     * Gets id from item on list
     * @param position
     *      position
     * @return
     *      id
     */
    @Override
    public long getItemId(int position) {
        return 0;
    }

    /**
     * Creates view for this list view
     *
     * @param position
     *      position in the list
     * @param convertView
     *      view
     * @param parent
     *      parent
     * @return
     *      converted view
     */
    @Override
    public View getView(int position, View convertView, final ViewGroup parent) {
        final ClassroomIndividual currentClassroomIndividual;

        if (getSearch()){
            currentClassroomIndividual = (ClassroomIndividual) searchIndividuals.get(position-1);
        }
        else {
            currentClassroomIndividual = (ClassroomIndividual) getItem(position);
        }

        final LayoutInflater classroomInflator =
                (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        convertView = classroomInflator.inflate(R.layout.user_list_layout, null);

        TextView classroomIndividualNameTextView =
                convertView.findViewById(R.id.userIndividualNameTextView);

        classroomIndividualNameTextView.setText(currentClassroomIndividual.getName());

        return convertView;
    }

    /**
     * Sets searching variable
     *
     * @param bool
     *      boolean
     */
    public void setSearching(boolean bool){
        searching = bool;
        this.notifyDataSetChanged();
    }

    /**
     * Gets search variable
     * @return
     */
    public boolean getSearch(){
        return this.searching;
    }

    /**
     * Returns an array list of this adapter
     */
    public List<ClassroomIndividual> getArrayList(){
        return allUserIndividuals;
    }

    /**
     * Gets searched individuals
     * @return
     *      results
     */
    public List<ClassroomIndividual> getSearchIndividuals() {
        return searchIndividuals;
    }

    /**
     * Sets searchIndividuals
     * @param searchIndividuals
     *      searchIndividuals
     */
    public void setSearchIndividuals(List<ClassroomIndividual> searchIndividuals) {
        this.searchIndividuals = searchIndividuals;
    }
}
