package com.example.labassistant.app.Chat;

/**
 *
 * Message object that is used in the chat fragment
 *
 * @author Marco Yepez
 * @author Joe Herrera
 */
public class Message {

    private int id;
    private String name;
    private String content;
    private String timeStamp;

    /**
     * Creates the Message object
     *
     * @param givenId
     *      Given id
     * @param givenName
     *      Given name
     * @param givenContent
     *      Given content
     * @param givenTimeStamp
     *      Given Time
     */
    public Message(int givenId, String givenName, String givenContent, String givenTimeStamp){
        id = givenId;
        name = givenName;
        content = givenContent;
        timeStamp = givenTimeStamp;
    }

    /**
     * Gets the name involved in the Message object
     *
     * @return
     *      name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the content involved in the Message object
     *
     * @return
     *      content
     */
    public String getContent() {
        return content;
    }

    /**
     * Gets the timeStamp involved in the Message object
     *
     * @return
     *      time
     */
    public String getTimeStamp() {
        return timeStamp;
    }

    /**
     * Gets the id involved in the Message object
     *
     * @return
     *      id
     */
    public int getId() {
        return id;
    }

    /**
     * Gets the tag involved in the Message object
     *
     * @return
     *      tag
     */
    public String getTag(){
        return name + ", " + timeStamp;
    }
}
