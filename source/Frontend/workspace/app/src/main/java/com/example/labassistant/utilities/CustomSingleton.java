package com.example.labassistant.utilities;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

/**
 * Custom made singleton for project
 * @author Marco Yepez
 * @author Joe Herrera
 */
public class CustomSingleton {

    private static CustomSingleton mInstance;
    private RequestQueue requestQueue;
    private static Context mCtx;

    /**
     * Constructor for CustomSingleton
     *
     * @param context
     *      context
     */
    private CustomSingleton(Context context){
        mCtx = context;
        requestQueue = getRequestQueue();
    }

    /**
     * Gets requestQueue
     * @return
     *      requestQueue
     */
    public RequestQueue getRequestQueue(){
        if(requestQueue==null){
            requestQueue = Volley.newRequestQueue(mCtx.getApplicationContext());
        }
        return requestQueue;
    }

    /**
     * Gets Instance
     *
     * @param context
     *      context
     * @return
     *      instance
     */
    public static synchronized CustomSingleton getInstance(Context context){
        if (mInstance == null) {
            mInstance = new CustomSingleton(context);
        }
        return mInstance;
    }

    /**
     * Add to queue
     *
     * @param request
     *      reques
     * @param <T>
     *     generic
     */
    public<T> void addToRequestQueue(Request request){
        requestQueue.add(request);
    }
}
