package com.example.labassistant.app.Queue;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.labassistant.R;
import com.example.labassistant.utilities.QueueListAdapter;

import org.java_websocket.client.WebSocketClient;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 *
 * @author Marco Yepez
 * @author Joe Herrera
 */
public class QueueAddPopup extends Fragment {

    /**
     * Creates QueueAddPopup object
     */
    public QueueAddPopup() {
        // Required empty public constructor
    }


    /**
     * Method called when class is created
     *
     * @param inflater
     *      inflater
     * @param container
     *      container
     * @param savedInstanceState
     *      savedInstanceState
     * @return
     *      inflate
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View inflate = inflater.inflate(R.layout.fragment_queue_add_popup, container, false);
        return inflate;
    }

    /**
     * Method generates the queue add pop-up view
     *
     * @param view
     *      view
     * @param queueListAdapter
     *      queue list adapter
     * @param currentReasons
     *      current reason
     * @param webSocketClient
     *      websocket client
     */
    public static void generateQueueAddPopup(View view, final QueueListAdapter queueListAdapter, final ArrayList<String> currentReasons, final WebSocketClient webSocketClient){
        LayoutInflater layoutInflater = LayoutInflater.from(view.getContext());
        final View popupSortView = layoutInflater.inflate(R.layout.fragment_queue_add_popup, null);
        int width = LinearLayout.LayoutParams.WRAP_CONTENT;
        final int height = LinearLayout.LayoutParams.WRAP_CONTENT;
        final PopupWindow popupWindow = new PopupWindow(popupSortView, width, height, true);
        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);

        // init
        final TextView cancelTextView = popupSortView.findViewById(R.id.queueAddCancelTextView);
        Button checkoffBtn = (Button) popupSortView.findViewById(R.id.checkoffBtn);
        Button questionBtn = (Button) popupSortView.findViewById(R.id.questionBtn);

        //Get the user information
        SharedPreferences sharedPreferences = view.getContext().getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        final String username = sharedPreferences.getString("username", "Test123");
        final String firstname = sharedPreferences.getString("firstname", "Jane");
        final String lastname = sharedPreferences.getString("lastname", "Doe");

        // OnClick Listener for checkoff button
        checkoffBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!currentReasons.contains("checkoff")){
                    String reason = "checkoff";
                    QueueIndividual queueIndividual = new QueueIndividual(firstname, lastname,
                            reason, username);
//                    queueListAdapter.addQueueIndividual(queueIndividual);
                    webSocketClient.send("AddQueue:" + username+ "," + reason);
//                    webSocketClient.send("queue");
//                    queueListAdapter
//                    queueListAdapter.notifyDataSetChanged();
                    popupWindow.dismiss();
                }
                else{
                    Toast.makeText(view.getContext(), "You are already in queue for a checkoff", Toast.LENGTH_SHORT).show();
                }

            }
        });

        // OnClick Listener for the question button
        questionBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!currentReasons.contains("question")){
                    String reason = "question";
                    QueueIndividual queueIndividual = new QueueIndividual(firstname, lastname,
                            reason, username);
//                    queueListAdapter.addQueueIndividual(queueIndividual);
                    webSocketClient.send("AddQueue:" + username+ "," + reason);
//                    webSocketClient.send("queue");
//                    queueListAdapter.notifyDataSetChanged();
                    popupWindow.dismiss();
                }
                else{
                    Toast.makeText(view.getContext(), "You are already in queue for a question", Toast.LENGTH_SHORT).show();
                }
            }
        });

        // OnClick Listener for cancel button
        cancelTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });
    }
}
