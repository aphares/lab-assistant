package com.example.labassistant.utilities;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.labassistant.R;
import com.example.labassistant.app.Chat.Message;

import java.util.ArrayList;
import java.util.List;

/**
 * List adapter for messages
 * @author Marco Yepez
 * @author Joe Herrera
 */
public class MessagesAdapter extends BaseAdapter {

    List<Message> allMessage = new ArrayList<Message>();
    Context context;

    /**
     * Constructor for ClassworkListAdapter
     *
     * @param givenContext
     *      context
     */
    public MessagesAdapter(Context givenContext){
        context = givenContext;
    }

    /**
     * Adds message to list
     *
     * @param givenMessage
     *      message
     */
    public void addMessage(Message givenMessage){
        allMessage.add(givenMessage);
        notifyDataSetChanged();
    }

    /**
     * Clears list
     */
    public void clearMessages(){
        allMessage.clear();
    }

    /**
     * Gets count of list
     *
     * @return
     *      count
     */
    @Override
    public int getCount() {
        return allMessage.size();
    }

    /**
     * Gets item from list
     *
     * @param position
     *      position
     * @return
     *      item
     */
    @Override
    public Message getItem(int position) {
        return allMessage.get(position);
    }

    /**
     * id for item
     *
     * @param position
     *      position in the list
     * @return
     *      id
     */
    @Override
    public long getItemId(int position) {
        return allMessage.get(position).getId();
    }

    /**
     * Creates view for this list view
     *
     * @param position
     *      position in the list
     * @param convertView
     *      view
     * @param parent
     *      parent
     * @return
     *      converted view
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Message currentMessage = getItem(position);

        LayoutInflater messageInflater = (LayoutInflater)
                context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        convertView = messageInflater.inflate(R.layout.messages_layout, null);

        TextView messageContentTextView =
                (TextView) convertView.findViewById(R.id.messageContentTextView);
        TextView messageTagTextView =
                (TextView) convertView.findViewById(R.id.messageTagTextView);

        messageContentTextView.setText(currentMessage.getContent());
        messageTagTextView.setText(currentMessage.getTag());

        return convertView;
    }

    public void clearList() {
        allMessage.clear();
    }
}
