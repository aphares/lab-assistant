package com.example.labassistant.utilities;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.example.labassistant.R;
import com.example.labassistant.app.Settings.Classroom.ClassroomIndividual;
import com.example.labassistant.app.Settings.Classroom.SettingsClassroomStudentFragment;
import com.example.labassistant.app.Settings.Classroom.SettingsClassroomTaFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Classroom Adapter for list views
 *
 * @author Marco Yepez
 * @author Joe Herrera
 */
public class ClassroomListAdapter extends BaseAdapter {
    private List<ClassroomIndividual> allIndividuals = new ArrayList<ClassroomIndividual>();
    private Context context;
    private boolean showAllCheckboxes = false;
    private boolean selectedAllCheckboxes = false;
    private String parentFragment;

    /**
     * Constructor for ClassroomListAdapter
     *
     * @param givenContext
     *      context
     */
    public ClassroomListAdapter(Context givenContext){
        context = givenContext;
    }

    /**
     * Constructor for ClassroomListAdapter
     *
     * @param givenContext
     *      context
     * @param givenParentFragement
     *      fragement
     */
    public ClassroomListAdapter(Context givenContext, String givenParentFragement){
        context = givenContext;
        parentFragment = givenParentFragement;
    }

    /**
     * Adds individual to list
     *
     * @param givenClassroomIndividual
     *      individual
     */
    public void addIndividual(ClassroomIndividual givenClassroomIndividual){
        allIndividuals.add(givenClassroomIndividual);
        notifyDataSetChanged();
    }

    /**
     * Clears list
     */
    public void clearList(){
        allIndividuals.clear();
    }

    /**
     * Gets list count
     *
     * @return
     *      count
     */
    @Override
    public int getCount() {
        return allIndividuals.size();
    }

    /**
     * Gets item from list
     *
     * @param position
     *      position in list
     * @return
     *      item
     */
    @Override
    public Object getItem(int position) {
        return allIndividuals.get(position);
    }

    /**
     * Get item id
     * @param position
     *      position in list
     * @return
     *      id
     */
    @Override
    public long getItemId(int position) {
        return 0;
    }

    /**
     * Creates custom made view in list view
     *
     * @param position
     *      position
     * @param convertView
     *      view
     * @param parent
     *      parent
     * @return
     *      converted view
     */
    @Override
    public View getView(int position, View convertView, final ViewGroup parent) {
        final ClassroomIndividual currentClassroomIndividual = (ClassroomIndividual) getItem(position);

        final LayoutInflater classroomInflator =
                (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        convertView = classroomInflator.inflate(R.layout.classroom_list_layout, null);

        TextView classroomIndividualSectionNumberTextView =
                convertView.findViewById(R.id.classroomIndividualSectionNumberTextView);
        TextView classroomIndividualNameTextView =
                convertView.findViewById(R.id.classroomIndividualNameTextView);
        final CheckBox classroomIndividualSelectedCheckbox =
                convertView.findViewById(R.id.classroomIndividualSelectCheckBox);

        classroomIndividualSectionNumberTextView.setText(
                currentClassroomIndividual.sectionNumberToString());
        classroomIndividualNameTextView.setText(currentClassroomIndividual.getName());
        classroomIndividualSelectedCheckbox.setChecked(currentClassroomIndividual.isCheckBox());

        final View finalConvertView = convertView;
        classroomIndividualSelectedCheckbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(currentClassroomIndividual.isCheckBox()){
                    selectedAllCheckboxes = false;
                    currentClassroomIndividual.setCheckBox(false);
                }
                else {
                    currentClassroomIndividual.setCheckBox(true);
                }

                if(isAllCheckboxesUnmarked()){
                    showAllCheckboxes = false;
                    if(parentFragment.equals("student")){
                        SettingsClassroomStudentFragment.removeDeleteView(false);
                    }
                    else if(parentFragment.equals("ta")){
                        SettingsClassroomTaFragment.removeDeleteView(false);
                    }
                }
                notifyDataSetChanged();
            }
        });

        evaluateCheckbox(classroomIndividualSelectedCheckbox, currentClassroomIndividual);

        return convertView;
    }

    /**
     * Evaluates checkboxes
     *
     * @param classroomIndividualSelectedCheckbox
     *      classroom individual selected checkbox
     * @param currentClassroomIndividual
     *      current classroom individual
     */
    private void evaluateCheckbox(CheckBox classroomIndividualSelectedCheckbox,
                                 ClassroomIndividual currentClassroomIndividual){

        if(currentClassroomIndividual.isCheckBox()){
            classroomIndividualSelectedCheckbox.setChecked(true);
        }
        else {
            classroomIndividualSelectedCheckbox.setChecked(false);
        }

        if(showAllCheckboxes){
            classroomIndividualSelectedCheckbox.setVisibility(View.VISIBLE);
        }
        else {
            classroomIndividualSelectedCheckbox.setVisibility(View.INVISIBLE);
        }
        notifyDataSetChanged();
    }

    /**
     * Checks if all checkboxes are marked
     *
     * @return
     *      boolean
     */
    public boolean isAllCheckboxesUnmarked(){
        for(int i = 0; i < getCount(); i++){
            ClassroomIndividual classroomIndividual = (ClassroomIndividual) getItem(i);
            if(classroomIndividual.isCheckBox()){
                return false;
            }
        }
        return true;
    }

    /**
     * Returns an array list of this adapter
     */
    public List<ClassroomIndividual> getArrayList(){
        return allIndividuals;
    }

    /**
     * Gets selectedAllCheckboxes
     * @return
     *      selectedAllCheckboxes
     */
    public boolean isSelectedAll() {
        return selectedAllCheckboxes;
    }

    /**
     * sets selectedAllCheckboxes
     * @param selectedAll
     *      selectedAllCheckboxes
     */
    public void setSelectedAll(boolean selectedAll) {
        if(selectedAll){
            doClassroomIndividualSetCheckBoxesTrue();
        }
        this.selectedAllCheckboxes = selectedAll;
    }

    /**
     *  Returns showAllCheckboxes
     *
     * @return
     *      showAllCheckboxes
     */
    public boolean isShowAllCheckboxes() {
        return showAllCheckboxes;
    }

    /**
     * sets showAllCheckboxes
     * @param showAllCheckboxes
     *      showAllCheckboxes
     */
    public void setShowAllCheckboxes(boolean showAllCheckboxes) {
        this.showAllCheckboxes = showAllCheckboxes;
    }

    /**
     * Sets all checkboxes to true
     */
    private void doClassroomIndividualSetCheckBoxesTrue(){
        for(int i = 0; i < getCount(); i++){
            ClassroomIndividual classroomIndividual = (ClassroomIndividual) getItem(i);
            classroomIndividual.setCheckBox(true);
        }
    }

    /**
     * Gets all checkboxes that are true
     * @return
     *      true checkboxes
     */
    public List<Integer> getAllTrueCheckbox(){
        List<Integer> list = new ArrayList<>();
        for(int i = 0; i < getCount(); i++){
            ClassroomIndividual classroomIndividual = (ClassroomIndividual) getItem(i);
            if(classroomIndividual.isCheckBox()){
                list.add(i);
            }
        }
        return list;
    }

    /**
     * Sets all checkboxes to false
     */
    public void doClassroomIndividualSetCheckBoxesFalse(){
        for(int i = 0; i < getCount(); i++){
            ClassroomIndividual classroomIndividual = (ClassroomIndividual) getItem(i);
            classroomIndividual.setCheckBox(false);
        }
    }
}
