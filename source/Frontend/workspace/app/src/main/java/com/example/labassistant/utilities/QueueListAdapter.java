package com.example.labassistant.utilities;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.labassistant.R;
import com.example.labassistant.app.Queue.QueueIndividual;

import java.util.ArrayList;
import java.util.List;

/**
 * List adapter for queue
 * @author Marco Yepez
 * @author Joe Herrera
 */
public class QueueListAdapter extends BaseAdapter {
    private List<QueueIndividual> queueIndividualList = new ArrayList<>();
    private Context context;
    SharedPreferences sharedPreferences;
    String currentUser;

    /**
     * Constructor for QueueListAdapter
     * @param givenContext
     *      context
     */
    public QueueListAdapter(Context givenContext){
        context = givenContext;
        sharedPreferences = context.getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        currentUser = sharedPreferences.getString("username", "Invalid");
    }

    /**
     * Adds individual to list
     * @param queueIndividual
     */
    public void addQueueIndividual(QueueIndividual queueIndividual){
        queueIndividualList.add(queueIndividual);
    }

    /**
     * Gets list
     * @return
     *      list
     */
    public List<QueueIndividual> getList(){
        return queueIndividualList;
    }

    /**
     * Checks is someone has been selected
     *
     * @return
     *      boolean
     */
    public boolean isOneSelectedFromQueueList(){
        for(QueueIndividual queueIndividual : queueIndividualList){
            if(queueIndividual.isSelected()){
                return true;
            }
        }
        return false;
    }

    /**
     * Deselects individuals
     */
    public void deselectIndividuals(){
        for(QueueIndividual queueIndividual : queueIndividualList){
            queueIndividual.setSelected(false);
        }
    }

    /**
     * Removes item
     *
     * @param position
     *      position in list
     */
    public void removeItem(int position){
        queueIndividualList.remove(getItem(position));
    }

    /**
     * clears list
     */
    public void clearList(){
        queueIndividualList.clear();
    }

    /**
     * gets list count
     * @return
     *      count
     */
    @Override
    public int getCount() {
        return queueIndividualList.size();
    }

    /**
     * Gets item in list
     *
     * @param position
     *      position in list
     * @return
     *      item
     */
    @Override
    public Object getItem(int position) {
        return queueIndividualList.get(position);
    }

    /**
     * Gets item from list
     * @param position
     *      position
     * @return
     *      id
     */
    @Override
    public long getItemId(int position) {
        return 0;
    }

    /**
     * Creates view for this list view
     *
     * @param position
     *      position in the list
     * @param convertView
     *      view
     * @param parent
     *      parent
     * @return
     *      converted view
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        QueueIndividual currentQueueIndividual = (QueueIndividual) getItem(position);
        final LayoutInflater classroomInflator =
                (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        convertView = classroomInflator.inflate(R.layout.queue_list_layout, null);

        TextView queueIndividualNameTextView =
                convertView.findViewById(R.id.queueIndividualNameTextView);
        ImageView queueIndividualReasonImageView =
                convertView.findViewById(R.id.queueIndividualReasonImageView);
        LinearLayout queueIndividualLinearLayout =
                convertView.findViewById(R.id.queueListLinearLayout);

        queueIndividualNameTextView.setText(currentQueueIndividual.getFullName());

        if(currentQueueIndividual.getReason().equals("question")){
            queueIndividualReasonImageView.setImageResource(R.drawable.ic_question_mark);
        }
        else if(currentQueueIndividual.getReason().equals("checkoff")){
            queueIndividualReasonImageView.setImageResource(R.drawable.ic_check_black_24dp);
        }

        if(currentQueueIndividual.isSelected()){
            System.out.println();
            queueIndividualLinearLayout.setBackgroundColor(Color.parseColor("#f2e8e8"));
        }
        else{
            queueIndividualLinearLayout.setBackgroundColor(Color.parseColor("#ffffff"));
        }

        if(currentQueueIndividual.getUsername().equals(currentUser)){
            queueIndividualNameTextView.setTypeface(null, Typeface.BOLD);
        }
        return convertView;
    }
}
