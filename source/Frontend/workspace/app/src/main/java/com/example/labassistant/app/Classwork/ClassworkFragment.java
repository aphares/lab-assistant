package com.example.labassistant.app.Classwork;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.labassistant.R;
import com.example.labassistant.utilities.ClassworkListAdapter;
import com.example.labassistant.utilities.CustomSingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


/**
 * A simple {@link Fragment} subclass.
 *
 * @author Marco Yepez
 * @author Joe Herrera
 */
public class ClassworkFragment extends Fragment {
    String classworkListUrl = "http://cs309-rr-2.misc.iastate.edu:8080/classes";
    private Classwork classwork;
    private ClassworkListAdapter classworkListAdapter;

    private ListView clasworkListView;

    /**
     * Creates the ClassworkFragment object
     */
    public ClassworkFragment() {
        // Required empty public constructor
    }


    /**
     * Runs method when creating the view
     *
     * @param inflater
     *      Given inflater
     * @param container
     *      Given container
     * @param savedInstanceState
     *      Given savedInstanceState
     * @return
     *      Inflater for ClassworkFragment
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View inflate = inflater.inflate(R.layout.fragment_classwork, container, false);

        classworkListAdapter = new ClassworkListAdapter(getContext());
        clasworkListView = inflate.findViewById(R.id.classworkListView);

        getAllClasswork();
        return inflate;
    }

    /**
     * Updates the classwork view with new classwork
     */
    private void getAllClasswork() {
        // The following is used to demonstrate the view of the ListView to the developer.
        // This can be deleted or commented out
//        classwork = new Classwork("2009-04-09 Classwork (Current Week)",
//                "Checkpoint 01: Complete for loop", "Checkpoint 02: " +
//                "Complete recursion");
//        classworkListAdapter.addClasswork(classwork);
//        classwork = new Classwork("2009-04-02 Classwork (Previous Week)",
//                "Checkpoint 01: Complete another for loop", "Checkpoint 02: " +
//                "Complete another recursion");
//        classworkListAdapter.addClasswork(classwork);
//        classwork = new Classwork("2009-03-26 Classwork (Few Weeks Ago)",
//                "Checkpoint 01: Complete some random task", "Checkpoint 02: " +
//                "Complete set up of U:Drive");
//        classworkListAdapter.addClasswork(classwork);
//        classwork = new Classwork("2009-03-19 Classwork (Few Weeks Ago)",
//                "Checkpoint 01: Complete another random task", "Checkpoint 02: " +
//                "Complete a while loop");
//        classworkListAdapter.addClasswork(classwork);
//        clasworkListView.setAdapter(classworkListAdapter);

        // TODO Complete the queue to get all the classwork list from backend when Andrew is done
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                classworkListUrl, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    String embedded_obj = response.getString("_embedded");
                    JSONObject jsonObject = new JSONObject(embedded_obj);
                    JSONArray users_table = jsonObject.getJSONArray("classroomList");

                    int index = 0;
                    while(index < users_table.length()){
                        JSONObject currentObject = users_table.getJSONObject(index);

                        String title = currentObject.getString("date");
                        String checkpoint01 = currentObject.getString("checkpoint1");
                        String checkpoint02 = currentObject.getString("checkpoint2");

                        classwork = new Classwork(title, checkpoint01, checkpoint02);
                        classworkListAdapter.addClasswork(classwork);
                        index++;
                    }
                    clasworkListView.setAdapter(classworkListAdapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast toast = Toast.makeText(getContext(), "Failed to get classwork list",
                        Toast.LENGTH_LONG);
                toast.show();
                error.printStackTrace();
            }
        });
        CustomSingleton.getInstance(getContext()).addToRequestQueue(jsonObjectRequest);
    }

}
