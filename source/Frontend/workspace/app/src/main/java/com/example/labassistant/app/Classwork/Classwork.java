package com.example.labassistant.app.Classwork;

/**
 * Classwork object that is used in the chat fragment
 *
 * @author Marco Yepez
 * @author Joe Herrera
 */
public class Classwork {
    private String title, checkpoint01, checkpoint02;

    /**
     * Creates the Message object
     *
     * @param title
     *      given title
     * @param checkpoint01
     *      given checkpoint 01
     * @param checkpoint02
     *      given checkpoint 02
     */
    public Classwork(String title, String checkpoint01, String checkpoint02){
        this.title = title;
        this.checkpoint01 = checkpoint01;
        this.checkpoint02 = checkpoint02;
    }

    /**
     * Gets the title involved in the Message object
     *
     * @return
     *      title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the title involved in the Message object
     *
     * @param title
     *      title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Gets checkpoint 01 involved in the Message object
     *
     * @return
     *      checkpoint 01
     */
    public String getCheckpoint01() {
        return checkpoint01;
    }

    /**
     * Sets checkpoint 01 involved in the Message object
     *
     * @param checkpoint01
     *      checkpoint 01
     */
    public void setCheckpoint01(String checkpoint01) {
        this.checkpoint01 = checkpoint01;
    }

    /**
     * Gets checkpoint 02 involved in the Message object
     *
     * @return
     *      checkpoint 02
     */
    public String getCheckpoint02() {
        return checkpoint02;
    }

    /**
     * Sets checkpoint 02 involved in the Message object
     *
     * @param checkpoint02
     *      checkpoint 02
     */
    public void setCheckpoint02(String checkpoint02) {
        this.checkpoint02 = checkpoint02;
    }
}
