package com.example.labassistant.utilities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.support.constraint.ConstraintLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.labassistant.R;
import com.example.labassistant.app.Activities.MainActivity;
import com.example.labassistant.app.Settings.Classroom.ClassroomIndividual;
import com.example.labassistant.app.Settings.Classroom.SettingsClassroomSort;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Creates pop-ups
 */
public class PopUpUtils {

    private static int lastButtonState;

    private static String studentListDirectory = "http://cs309-rr-2.misc.iastate.edu:8080/users";
    private static ClassroomIndividual classroomIndividual;
    private static UserListAdapter userListAdapter;
    private static ListView searchResultListView;
    private static List<ClassroomIndividual> allUsers;
    private static ClassroomIndividual selectedUser;

    private static String addingStudentDirectory = "http://cs309-rr-2.misc.iastate.edu:8080/students";
    private static String addingTADirectory = "http://cs309-rr-2.misc.iastate.edu:8080/tas";

    // 0 for student
    // 1 for Ta
    private static int studentOrTa;


    /**
     * Generates a popUp View for add
     *
     * @param view
     * @param layoutInflater
     */
    @SuppressLint("ClickableViewAccessibility")
    public static void generateAddPopup(final View view, LayoutInflater layoutInflater) {
        final View popupSortView = layoutInflater.inflate(R.layout.fragment_settings_classroom_add, null);

        // init
        final TextView cancelTextView = popupSortView.findViewById(R.id.queueAddCancelTextView);
        final TextView okTextView = popupSortView.findViewById(R.id.okTextView);
        final EditText searchBarEditText = popupSortView.findViewById(R.id.addSearchEditText);
        final TextView newUserData = popupSortView.findViewById(R.id.newUserDataTextView);

        // Drawable search and x views
        final Drawable xDrawable = view.getResources().getDrawable(R.drawable.ic_clear_black_24dp);
        xDrawable.setBounds(0, 0, xDrawable.getIntrinsicWidth(), xDrawable.getIntrinsicHeight());
        final Drawable searchDrawable = view.getResources().getDrawable(R.drawable.ic_search_black_24dp);
        searchDrawable.setBounds(0, 0, searchDrawable.getIntrinsicWidth(), searchDrawable.getIntrinsicHeight());

        searchResultListView = popupSortView.findViewById(R.id.addSearchListView);
        userListAdapter = new UserListAdapter(view.getContext());

        int width = ConstraintLayout.LayoutParams.WRAP_CONTENT;
        final int height = ConstraintLayout.LayoutParams.WRAP_CONTENT;
        final PopupWindow popupWindow = new PopupWindow(popupSortView, width, height, true);
        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);
        getAllUsers(popupSortView);

        //Init radio button group with each radio button
        RadioGroup radioGroup = (RadioGroup) popupSortView.findViewById(R.id.addRadioGroup);

        RadioButton studentRadioButton = (RadioButton) popupSortView.findViewById(R.id.studentRadioButton);
        RadioButton taRadioButton = (RadioButton) popupSortView.findViewById(R.id.taRadioButton);

        //Checking last button state and highlighting the corresponding radio button
        SharedPreferences sharedPreferences = view.getContext().getSharedPreferences("preferences", Context.MODE_PRIVATE);
        lastButtonState = sharedPreferences.getInt("RADIOBUTTON", 2);


        // OnClick listener for the Radio Buttons
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.studentRadioButton:
                        studentOrTa = 0;
                        break;
                    case R.id.taRadioButton:
                        studentOrTa = 1;
                        break;
                }
            }
        });

        // OnClick listener for list view
        searchResultListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ClassroomIndividual classroomIndividual = (ClassroomIndividual)
                        userListAdapter.getItem(position);
                selectedUser = classroomIndividual;
                //TODO implement things to do with this classroom individual
                newUserData.setVisibility(View.VISIBLE);
                searchResultListView.setVisibility(View.GONE);

                //Formatting xml
                String format = selectedUser.getName() + " \n" + "Email: " + selectedUser.getEmail();
                newUserData.setText(format);

                // minimize the keyboard
                MainActivity.hideKeyboardFrom(view.getContext(), searchBarEditText);

            }
        });


        // On Touch listener for the clear button on the search bar
        searchBarEditText.setOnTouchListener(new View.OnTouchListener() {
            final int DRAWABLE_LEFT = 0;
            final int DRAWABLE_TOP = 1;
            final int DRAWABLE_RIGHT = 2;
            final int DRAWABLE_BOTTOM = 3;
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    int leftEdgeOfRightDrawable = searchBarEditText.getRight()
                            - searchBarEditText.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width();
                    // when EditBox has padding, adjust leftEdge like
                    // leftEdgeOfRightDrawable -= getResources().getDimension(R.dimen.edittext_padding_left_right);
                    if (event.getRawX() >= leftEdgeOfRightDrawable) {
                        // clicked on clear icon
                        searchBarEditText.setText("");
                        return true;
                    }
                }
                return false;
            }
        });

        // Text change listener for search bar
        searchBarEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (newUserData.getVisibility() == View.VISIBLE){
                    newUserData.setVisibility(View.INVISIBLE);
                    searchResultListView.setVisibility(View.VISIBLE);
                }
                String searchString = s.toString().trim();
                if (s.length() == 0) {
                    searchBarEditText.setCompoundDrawables(null, null, searchDrawable, null);
                    getAllUsers(popupSortView);
                } else if (s.charAt(s.length() - 1) == '\n') {
                    searchBarEditText.setCompoundDrawables(null, null, xDrawable, null);
                    searchBarEditText.setText(searchString);

                    //TODO Come up with a keyboard hiding solution
                    MainActivity.hideKeyboardFrom(view.getContext(), searchBarEditText);
                    searchBarEditText.clearFocus();
                    searchBarEditText.setSelection(searchBarEditText.getText().length());
                } else {
                    searchBarEditText.setCompoundDrawables(null, null, xDrawable, null);
                    search(popupSortView, searchString);
                }
            }
        });


        // OnClick listener for "Cancel"
        cancelTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });

        // OnClick listener for "ok"
        okTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                SettingsClassroomAdd.addUser
                if(selectedUser != null){
                    addUser(popupSortView, selectedUser);
                }
                popupWindow.dismiss();
            }
        });
    }

    /**
     * Gets all the users from backend
     *
     * @param view
     *      view
     */
    public static void getAllUsers(final View view) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET,
                studentListDirectory, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    String embedded_obj = response.getString("_embedded");
                    JSONObject jsonObject = new JSONObject(embedded_obj);
                    JSONArray users_table = jsonObject.getJSONArray("user_tableList");

                    //Clears the adapter
                    userListAdapter = new UserListAdapter(view.getContext());

                    int index = 0;
                    while (index < users_table.length()) {
                        JSONObject currentObject = users_table.getJSONObject(index);

                        String username = currentObject.getString("username");
                        String firstName = currentObject.getString("firstname");
                        String lastname = currentObject.getString("lastname");
                        String email = currentObject.getString("email");
                        int id = currentObject.getInt("id");

                        String fullName = lastname + ", " + firstName;

                        classroomIndividual = new ClassroomIndividual(username, firstName, lastname, email, id);
                        classroomIndividual.setUsername(username);
                        classroomIndividual.setEmail(email);
                        userListAdapter.addIndividual(classroomIndividual);
                        searchResultListView.setAdapter(userListAdapter);
                        index++;
                    }
                    allUsers = userListAdapter.getArrayList();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast toast = Toast.makeText(view.getContext(), "Failed to get users",
                        Toast.LENGTH_LONG);
                toast.show();
                error.printStackTrace();
            }
        });
        CustomSingleton.getInstance(view.getContext()).addToRequestQueue(jsonObjectRequest);
    }

    /**
     * Searches for user in search bar
     *
     * @param view
     *      view
     * @param search
     *      search
     */
    private static void search(View view, String search) {

        List<ClassroomIndividual> searchingUsers = new ArrayList<>();
        userListAdapter = new UserListAdapter(view.getContext());
        boolean isNumber = false;

        //Checks if the string is an id number or lastname
        if(search.matches(".*\\d+.*")){
            isNumber = true;
        }
        else {
            search = search.toLowerCase();
        }

        for (int i = 0; i < allUsers.size(); i++) {
            ClassroomIndividual classroomIndividual = allUsers.get(i);
            String temp;
            if(isNumber){
                temp = Integer.toString(classroomIndividual.getId());
            }
            else {
                temp = classroomIndividual.getLastname().toLowerCase();
            }

            if (temp.startsWith(search)) {
                userListAdapter.addIndividual(classroomIndividual);
            }
        }
        searchResultListView.setAdapter(userListAdapter);
    }

    /**
     * Generates the pop view for sort
     */
    public static void generateSortPopup(final View view, LayoutInflater layoutInflater) {
        // Init
        final View popupSortView = layoutInflater.inflate(R.layout.fragment_settings_classroom_sort, null);
        final TextView cancelTextView = popupSortView.findViewById(R.id.queueAddCancelTextView);
        final TextView okTextView = popupSortView.findViewById(R.id.okTextView);
        int width = LinearLayout.LayoutParams.WRAP_CONTENT;
        final int height = LinearLayout.LayoutParams.WRAP_CONTENT;
        final PopupWindow popupWindow = new PopupWindow(popupSortView, width, height, true);
        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);

        //Init radio button group with each radio button
        RadioGroup radioGroup = (RadioGroup) popupSortView.findViewById(R.id.sortRadioGroup);

        RadioButton ascendingAlphaRadioButton = (RadioButton) popupSortView.findViewById(R.id.ascendingAlphaRadioButton);
        RadioButton descendingAlphaRadioButton = (RadioButton) popupSortView.findViewById(R.id.descendingAlphaRadioButton);
        RadioButton ascendingSectionRadioButton = (RadioButton) popupSortView.findViewById(R.id.ascendingSectionRadioButton);
        RadioButton descendingSectionRadioButton = (RadioButton) popupSortView.findViewById(R.id.descendingSectionRadioButton);

        //Checking last button state and highlighting the corresponding radio button
        SharedPreferences sharedPreferences = view.getContext().getSharedPreferences("preferences", Context.MODE_PRIVATE);
        lastButtonState = sharedPreferences.getInt("RADIOBUTTON", 2);

        switch (lastButtonState) {
            case 0:
                ascendingAlphaRadioButton.setChecked(true);
                break;
            case 1:
                descendingAlphaRadioButton.setChecked(true);
                break;
            case 2:
                ascendingSectionRadioButton.setChecked(true);
                break;
            case 3:
                descendingSectionRadioButton.setChecked(true);
                break;
        }


        // OnClick listener for the "Cancel" textView
        cancelTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });

        // OnClick listener for the "OK" textView
        okTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();

                //Stores the current selected radio button in pref
                SharedPreferences sharedPreferences = view.getContext().getSharedPreferences("preferences", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putInt("RADIOBUTTON", lastButtonState);
                editor.apply();
                SettingsClassroomSort.sortByDefault(view);
            }
        });

        // OnClick listener for the Radio Buttons
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.ascendingAlphaRadioButton:
                        lastButtonState = 0;
                        break;
                    case R.id.descendingAlphaRadioButton:
                        lastButtonState = 1;
                        break;
                    case R.id.ascendingSectionRadioButton:
                        lastButtonState = 2;
                        break;
                    case R.id.descendingSectionRadioButton:
                        lastButtonState = 3;
                        break;
                }
            }
        });
    }

    /**
     * Adds a user in popup view in Setting
     *
     * @param view
     *      view
     * @param classroomIndividual
     *      individual
     */
    public static void addUser(final View view, ClassroomIndividual classroomIndividual){
        String directoryToAdd = "";
//        if(classroomIndividual.getStudentOrTA().equals("ta".toLowerCase())){
//            directoryToAdd = addingTADirectory + "/" + classroomIndividual.getUsername();
//        }
//        else if(classroomIndividual.getStudentOrTA().equals("student".toLowerCase())){
//            directoryToAdd = addingStudentDirectory + "/" + classroomIndividual.getUsername();
//        }

        if (studentOrTa == 1) {
            directoryToAdd = addingTADirectory;
        } else if (studentOrTa == 0) {
            directoryToAdd = addingStudentDirectory;
        }

        JSONObject jsonObject = new JSONObject();
        try {
            //Assigning values to the jsonObject
            jsonObject.put("username", classroomIndividual.getUsername());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, directoryToAdd,
                jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                //Makes a failed error toast to the user
                Context context = view.getContext();
                CharSequence text = "Failed to Send";
                int duration = Toast.LENGTH_LONG;
                Toast toast = Toast.makeText(context, text, duration);
                toast.show();
                error.printStackTrace();
            }
        });

        CustomSingleton.getInstance(view.getContext()).addToRequestQueue(jsonObjectRequest);
    }
}
