package com.example.labassistant.app.Settings.Classroom;

import java.util.Comparator;

/**
 * ClassroomIndividual class
 *
 * @author Marco Yepez
 * @author Joe Herrera
 */
public class ClassroomIndividual {

    private String sectionNumber;
    private String username;
    private String name;
    private String email;
    private String lastname;
    private String firstname;
    private String studentOrTA;
    private int id;
    private boolean checkBox;

    /**
     * Constructor for class
     *
     * @param givenSectionNumber
     *      given section number
     * @param givenName
     *      given name
     * @param givenCheckBox
     *      given checkbox
     */
    public ClassroomIndividual(String givenSectionNumber, String givenName, boolean givenCheckBox){
        sectionNumber = givenSectionNumber;
        name = givenName;
        checkBox = givenCheckBox;
    }

    /**
     * Constructor for class
     *
     * @param givenSectionNumber
     *      given section number
     * @param givenName
     *      given name
     */
    public ClassroomIndividual(String givenSectionNumber, String givenName){
        sectionNumber = givenSectionNumber;
        name = givenName;
        checkBox = false;
    }

    /**
     * Constructor for class
     *
     * @param username
     *      username
     * @param firstname
     *      first name
     * @param lastname
     *      last name
     * @param email
     *      email
     * @param id
     *      id
     */
    public ClassroomIndividual(String username, String firstname, String lastname, String email, int id){
        this.username = username;
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.id = id;
        this.name = lastname + ", " + firstname;
    }

    /**
     * Gets the section number string
     *
     * @return
     *      section number string
     */
    public String sectionNumberToString() {
        return "S" + sectionNumber;
    }

    /**
     * Gets the section number
     *
     * @return
     *      section number
     */
    public int getSectionNumber(){
        return Integer.parseInt(sectionNumber);
    }

    /**
     * Sets the section number
     *
     * @param sectionNumber
     *      section number
     */
    public void setSectionNumber(String sectionNumber) {
        this.sectionNumber = sectionNumber;
    }

    /**
     * Set email
     * @param email
     *      email
     */
    public void setEmail(String email){
        this.email = email;
    }

    /**
     * Gets the email
     *
     * @return
     *      email
     */
    public String getEmail(){
        return this.email;
    }

    /**
     * Sets the id
     *
     * @param id
     *      id
     */
    public void setId(int id){
        this.id = id;
    }

    /**
     * Gets the id
     *
     * @return
     *      id
     */
    public int getId() {
        return this.id;
    }

    /**
     * Gets the name
     *
     * @return
     *      name
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the last name
     *
     * @return
     *      last name
     */
    public String getLastname(){
        return this.lastname;
    }

    /**
     * Gets the first name
     *
     * @return
     *      first name
     */
    public String getFirstname(){
        return this.firstname;
    }

    /**
     * Sets the name
     *
     * @param name
     *      name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Sets last name
     *
     * @param lastname
     *      last name
     */
    public void setLastname(String lastname){
        this.lastname = lastname;
    }

    /**
     * Gets the checkbox status
     *
     * @return
     *      checkboxes
     */
    public boolean isCheckBox() {
        return checkBox;
    }

    /**
     * Set the checkbox
     *
     * @param checkBox
     *      checkboxes
     */
    public void setCheckBox(boolean checkBox) {
        this.checkBox = checkBox;
    }

    /**
     * Comparator to sort section numbers order by descending order
     * Returns 1 if left is bigger
     */
    public static Comparator<ClassroomIndividual> individualSectionDescendingComparator = new Comparator<ClassroomIndividual>() {
        @Override
        public int compare(ClassroomIndividual individual1, ClassroomIndividual individual2) {
            int individualSection1 = individual1.getSectionNumber();
            int individualSection2 = individual2.getSectionNumber();
            return individualSection2 - individualSection1;
        }
    };

    /**
     * Comparator to sort section numbers order by ascending order
     * Returns 1 if left is bigger
     */
    public static Comparator<ClassroomIndividual> individualSectionAscendingComparator = new Comparator<ClassroomIndividual>() {
        @Override
        public int compare(ClassroomIndividual individual1, ClassroomIndividual individual2) {
            int individualSection1 = individual1.getSectionNumber();
            int individualSection2 = individual2.getSectionNumber();
            return individualSection1 - individualSection2;
        }
    };


    /**
     * Comparator to sort alphabetical order by descending order
     * Returns 1 if left is bigger
     */
    public static Comparator<ClassroomIndividual> individualAlphaDescendingComparator = new Comparator<ClassroomIndividual>() {
        @Override
        public int compare(ClassroomIndividual individual1, ClassroomIndividual individual2) {
            String individualName = individual1.getName();
            String individualName2= individual2.getName();
            return individualName2.compareToIgnoreCase(individualName);
        }
    };

    /**
     * Comparator to sort alphabetical order by ascending order
     * Returns 1 if left is bigger
     */
    public static Comparator<ClassroomIndividual> individualAlphaAscendingComparator = new Comparator<ClassroomIndividual>() {
        @Override
        public int compare(ClassroomIndividual individual1, ClassroomIndividual individual2) {
            String individualName = individual1.getName();
            String individualName2= individual2.getName();
            return individualName.compareToIgnoreCase(individualName2);
        }
    };

    /**
     * Gets the username
     *
     * @return
     *      username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets the username
     *
     * @param username
     *      username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Gets the studentOrTA
     *
     * @return
     *      studentOrTA
     */
    public String getStudentOrTA() {
        return studentOrTA;
    }

    /**
     * Sets the studentOrTA
     *
     * @param studentOrTA
     *      studentOrTA
     */
    public void setStudentOrTA(String studentOrTA) {
        this.studentOrTA = studentOrTA;
    }
}
